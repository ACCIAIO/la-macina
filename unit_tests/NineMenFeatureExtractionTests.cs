﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.evaluation;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class NineMenFeatureExtractionTests
    {
        private readonly ITestOutputHelper _output;

        public NineMenFeatureExtractionTests(ITestOutputHelper output)
        {
            _output = output;
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
        }

        [Fact]
        public void TestClosedMorris()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);
            Move m1 = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0);
            Move m2 = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 2);

            State s1 = s.Evolve(m1);
            State s2 = s.Evolve(m2);

            //Assert.True(s1.Features.ClosedMorris(Checker.WHITE));
            //Assert.False(s1.Features.ClosedMorris(Checker.BLACK));
            //Assert.False(s2.Features.ClosedMorris(Checker.WHITE));
            //Assert.False(s2.Features.ClosedMorris(Checker.BLACK));
        }

        [Fact]
        public void TestMorrisNumber()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);
            Move m1 = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0);
            Move m2a = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 2);

            Move m2b = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 1);
            Move m2c = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 0);

            Move m3a = Move.Of(Checker.BLACK, Phase.FIRST).To(0, 2, 1);
            Move m3b = Move.Of(Checker.BLACK, Phase.FIRST).To(1, 2, 1);

            State s1 = s.Evolve(m1);
            State s2a = s.Evolve(m2a);
            State s2c = s1.Evolve(m2a).Evolve(m2b).Evolve(m2c);
            State s3 = s2c.Evolve(m3a).Evolve(m3b);

            Assert.Single(s1.Features.GetMorrisLines(Checker.WHITE));//1)Test bianco sulla colonna esterna a sinistra
            Assert.Empty(s1.Features.GetMorrisLines(Checker.BLACK));
            Assert.Empty(s2a.Features.GetMorrisLines(Checker.WHITE));
            Assert.Empty(s2a.Features.GetMorrisLines(Checker.BLACK));
            Assert.Equal(2, s2c.Features.GetMorrisLines(Checker.WHITE).Count);//2) test bianco sulla riga interna in alto
            Assert.Empty(s2c.Features.GetMorrisLines(Checker.BLACK));
            Assert.Equal(2, s3.Features.GetMorrisLines(Checker.WHITE).Count); //come (2)
            Assert.Single(s3.Features.GetMorrisLines(Checker.BLACK));// 3) test nero sul ponte sud
        }

        [Fact]
        public void TestStuckPieces()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);
            Move m1 = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0);
            Move m2a = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 2);

            Move m2b = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 1);
            Move m2c = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 0);

            Move m3a = Move.Of(Checker.BLACK, Phase.FIRST).To(0, 2, 1);
            Move m3b = Move.Of(Checker.BLACK, Phase.FIRST).To(1, 2, 1);

            State s1 = s.Evolve(m1);
            State s2a = s.Evolve(m2a);
            State s2c = s1.Evolve(m2a).Evolve(m2b).Evolve(m2c);
            State s3 = s2c.Evolve(m3a).Evolve(m3b);

            Assert.Equal(1, s.Features.StuckOpponentPieces(Checker.WHITE));
            Assert.Equal(3, s.Features.StuckOpponentPieces(Checker.BLACK));
        }

        [Fact]
        public void TestPiecesNumber()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);
            Move m1 = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0);
            Move m2a = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 2);

            Move m2b = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 1);
            Move m2c = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 0);

            Move m3a = Move.Of(Checker.BLACK, Phase.FIRST).To(0, 2, 1);
            Move m3b = Move.Of(Checker.BLACK, Phase.FIRST).To(1, 2, 1);

            State s1 = s.Evolve(m1);
            State s2a = s.Evolve(m2a);
            State s2c = s1.Evolve(m2a).Evolve(m2b).Evolve(m2c);
            State s3 = s2c.Evolve(m3a).Evolve(m3b);

            Assert.Equal(6, s.Features.PiecesNumber(Checker.WHITE));
            Assert.Equal(6, s.Features.PiecesNumber(Checker.BLACK));
            Assert.Equal(7, s1.Features.PiecesNumber(Checker.WHITE));
            Assert.Equal(6, s1.Features.PiecesNumber(Checker.BLACK));
            Assert.Equal(10, s2c.Features.PiecesNumber(Checker.WHITE));
            Assert.Equal(6, s2c.Features.PiecesNumber(Checker.BLACK));
            Assert.Equal(10, s3.Features.PiecesNumber(Checker.WHITE));
            Assert.Equal(8, s3.Features.PiecesNumber(Checker.BLACK));
        }

        [Fact]
        public void TestOpenedMorris()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);
            Move m1 = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0);
            Move m2 = Move.Of(Checker.BLACK, Phase.FIRST).To(1, 0, 0).From(1, 1, 0);
            Move m3 = Move.Of(Checker.WHITE, Phase.FIRST).To(1, 1, 0).From(2, 1, 0);

            State s1 = s.Evolve(m1);
            State s2 = s1.Evolve(m2);
            State s3 = s2.Evolve(m3); //Apro un morris

            Assert.False(s1.Features.OpenedMorris(Checker.WHITE));
            Assert.False(s1.Features.OpenedMorris(Checker.BLACK));
            Assert.False(s2.Features.OpenedMorris(Checker.WHITE));
            Assert.False(s2.Features.OpenedMorris(Checker.BLACK));

            Assert.True(s3.Features.OpenedMorris(Checker.WHITE));
            Assert.False(s3.Features.OpenedMorris(Checker.BLACK));
        }

        [Fact]
        public void TestTwoPieces()
        {
            string toParse = "WOWOOOOOWWBBOBOOOOOOWWBW-First-6,6-3,3"; //Contiene due two pieces config per il bianco
            string toParse2 = "WOWOOOOOWWBBOBOOBOOOWWBW-First-6,6-3,3"; //Contiene due two pieces config per il bianco e uno su un ponte per il nero
            State s = State.Parse(toParse);
            State s2 = State.Parse(toParse2);

            Assert.Equal(2, s.Features.TwoPiecesConfigNumber(Checker.WHITE));
            Assert.Equal(0, s.Features.TwoPiecesConfigNumber(Checker.BLACK));
            Assert.Equal(2, s2.Features.TwoPiecesConfigNumber(Checker.WHITE));
            Assert.Equal(1, s2.Features.TwoPiecesConfigNumber(Checker.BLACK));
        }

        [Fact]
        public void TestThreePiecesConfigNumber()
        {
            string toParse = "WOWOOOOOWWBBOBOOOOOOWWBW-First-6,6-3,3"; //Contiene un three-pieces config per il bianco
            State s = State.Parse(toParse);

            Assert.Equal(1, s.Features.ThreePiecesConfigNumber(Checker.WHITE));
            Assert.Equal(0, s.Features.ThreePiecesConfigNumber(Checker.BLACK));
        }

        [Fact]
        public void TestDoubleMorris()
        {
            string toParse = "WWWOOOOOWWBBOBWOOOOOWWBW-First-6,6-3,3"; //Contiene un double morris per il bianco
            State s = State.Parse(toParse);

            Assert.True(s.Features.DoubleMorris(Checker.WHITE));
            Assert.False(s.Features.DoubleMorris(Checker.BLACK));
        }

        [Fact]
        public void TestGetLines()
        {
            string toParse = "WWWOOOOOWWBBOBWOOOOOWWBW-First-6,6-3,3"; //Contiene un double morris per il bianco
            State s = State.Parse(toParse);

            ISet<int> linesWithWhiteMorris = s.Features.GetMorrisLines(Checker.WHITE);
            ISet<int> linesWithBlackMorris = s.Features.GetMorrisLines(Checker.BLACK);

            Assert.Equal(3, linesWithWhiteMorris.Count);
            Assert.Empty(linesWithBlackMorris);

            Assert.Equal(new SortedSet<int>(new int[] { 0, 8, 15}), linesWithWhiteMorris); // linee 0, 8, 15
        }
    }
}
