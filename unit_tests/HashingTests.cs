﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.hashing;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class HashingTests
    {
        private readonly ITestOutputHelper _output;
        private readonly ZobristHashing _hashing;
        private readonly State _s1;

        public HashingTests(ITestOutputHelper output)
        {
            _output = output;
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
            _hashing = new ZobristHashing(9);
            _s1 = State.Parse("OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3");
        }

        [Fact]
        public void TestEqualHashing()
        {
            
            State s2 = State.Parse("OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3");

            Assert.Equal(_hashing.Hash(_s1), _hashing.Hash(s2));

        }

        [Fact]
        public void TestXor()
        {
            Move m1 = Move.Of(Checker.BLACK, Phase.FIRST).To(2, 0, 0);//.From(2, 0, 1).AndRemove(2, 2, 0);

            State s2 = _s1.Evolve(m1);

            System.DateTime t0 = System.DateTime.Now;
            long hash1 = _hashing.Hash(_s1);
            System.TimeSpan deltaT = System.DateTime.Now - t0;
            _output.WriteLine("Timespan Hash(ms): " + deltaT.TotalMilliseconds);

            long hash2Normal = _hashing.Hash(s2);

            t0 = System.DateTime.Now;
            long hash2Update = _hashing.UpdateHash(hash1, Phase.FIRST, _s1.OwnCheckers, _s1.OpponentCheckers, m1);
            deltaT = System.DateTime.Now - t0;
            _output.WriteLine("Timespan Update(ms): " + deltaT.TotalMilliseconds);

            Assert.Equal(hash2Normal , hash2Update);

            Assert.NotEqual(hash1, hash2Normal);

            _output.WriteLine("Hash 1: " + hash1);
            _output.WriteLine("Hash 2Normal: " + hash2Normal);
            _output.WriteLine("Hash 2Update: " + hash2Update);

        }

    }
}
