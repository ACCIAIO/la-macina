﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.hashing;
using Xunit;
using Xunit.Abstractions;
using la_macina.it.unibo.ai.macina.model.strategy;
using la_macina.it.unibo.ai.macina.model.strategy.implementations;
namespace unit_tests
{
    public class LBPBFTests
    {
		private readonly ITestOutputHelper _output;
		public LBPBFTests(ITestOutputHelper output)
		{
			_output = output;
		}

		[Fact]
		public void TestExpandNode()
		{
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
            MacinaEnvironment.GetInstance().DecisionTree = new DecisionGraph();

            var _hashing = new NineMenZobristHashing();
            var _s1 = State.Parse("OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3");
            var _algorithm = new LimitedBackPropagatedBestFirst(3, new StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic);
            var _tree = _algorithm.Tree;

            _output.WriteLine("ROOT");
			int nodesCount = _tree.NodesCount;
			_output.WriteLine("Count: {0}", nodesCount);
			Assert.Equal(0, _tree.MaxDepth);
			Assert.Equal(1, nodesCount);

			_output.WriteLine("Espando primo nodo:");
			_algorithm.ExpandNode();
			_output.WriteLine("Level: {0}", _algorithm.Tree.MaxDepth);
			nodesCount = _tree.NodesCount;
			_output.WriteLine("Count: {0}", nodesCount);
			Assert.Equal(1, _tree.MaxDepth);
			Assert.Equal(7, nodesCount);

			_algorithm.ExpandNode();
			_output.WriteLine("Level: {0}", _algorithm.Tree.MaxDepth);
			Assert.Equal(2, _tree.MaxDepth);

			_algorithm.ExpandNode();
			_output.WriteLine("Level: {0}", _algorithm.Tree.MaxDepth);
			Assert.Equal(3, _tree.MaxDepth);

			_algorithm.ExpandNode(); // ho raggiunto il la depthLimit
			_output.WriteLine("Level: {0}", _algorithm.Tree.MaxDepth);
			Assert.Equal(3, _tree.MaxDepth);
		}

		[Fact]
		public void TestSetNewCurrentState()
		{
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
            MacinaEnvironment.GetInstance().DecisionTree = new DecisionGraph();

            var _hashing = new NineMenZobristHashing();
            var _s1 = State.Parse("OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3");
            var _algorithm = new LimitedBackPropagatedBestFirst(3, new StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic);
            var _tree = _algorithm.Tree;

            _algorithm.ExpandNode();
			int numFrontierBeforeClean = _tree.MaxFrontier.Count + _tree.MinFrontier.Count + _tree.LastLevelFrontier.Count;
			State state = _tree.Root.Children[0].State;
			Move m = _algorithm.GetBestMove();
			_algorithm.SetCurrentState(_algorithm.GetCurrentState().Evolve(m));
			int numFrontierAfterClean = _tree.MaxFrontier.Count + _tree.MinFrontier.Count + _tree.LastLevelFrontier.Count;
			Assert.True(state.GetHashCode() == m.GetHashCode());
			Assert.True(numFrontierBeforeClean > numFrontierAfterClean);
			_algorithm.ExpandNode();
			_algorithm.ExpandNode();

		}

        [Fact]
        public void TestForMorrisMove()
        {
            MacinaEnvironment.CreateInstance(Checker.BLACK, 9);
            MacinaEnvironment.GetInstance().DecisionTree = new DecisionGraph();

            var _algorithm = new LimitedBackPropagatedBestFirst(3, new StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic);
            var _tree = _algorithm.Tree;

            State s1 = State.Parse("OOOOOOBOOOOOOOOBOOWOOOOO-First-8,7-1,2");
            State s2 = State.Parse("OOOOOOBOOOOOOOOBOOWOOWOO-First-7,7-2,2");
            Move m = s2.SynthetizeMoveFrom(s1);
            _algorithm.SetCurrentState(s1.Evolve(m));
            int i = 0;
            while(100 > i)
            {
                _algorithm.ExpandNode();
                i++;
            }

            Assert.False(true);
        }
	}
}
