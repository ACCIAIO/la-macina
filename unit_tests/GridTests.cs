using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.grid;
using System;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class GridTests
    {
        private readonly ITestOutputHelper _output;

        public GridTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void RingIstanceTest()
        {
            Ring r = new Ring();

            for(byte x = 0; x < 3; x++)
            {
                for(byte y = 0; y < 3; y++)
                {
                    if (1 == y && x == 1)
                        Assert.Equal(Checker.INVALID, r[x, y]);
                    else
                        Assert.Equal(Checker.NONE, r[x, y]);
                }
            }
        }
        
        [Fact]
        public void RingAdditionAndRemovalTest()
        {
            Ring r = new Ring();
            MoveStep put = new Put(Checker.WHITE, 0, 2, 0);
            r = r + put;
            Assert.Equal(Checker.WHITE, r[2, 0]);

            MoveStep remove = new Remove(StepTypes.FROM, 0, 2, 0);
            r = r + remove;
            Assert.Equal(Checker.NONE, r[2, 0]);
        }

        [Fact]
        public void ThreeMenConnectionsTest()
        {
            ThreeMenGrid grid = new ThreeMenGrid();

            var tuples = grid.ConnectionsOf(0, 0, 1);

            Assert.Equal(3, tuples.Count);
            Assert.True(tuples.Contains((0, 1, 1)));
            Assert.True(tuples.Contains((0, 0, 0)));
            Assert.True(tuples.Contains((0, 0, 2)));

            tuples = grid.ConnectionsOf(0, 1, 1);
            Assert.Equal(8, tuples.Count);
        }

        [Fact]
        public void ThreeMenFreeConnectionsTest()
        {
            IGrid grid = new ThreeMenGrid();
            grid = grid.Apply(Move.Of(Checker.WHITE, Phase.FIRST).To(0, 0, 0));

            var tuples = grid.FreeConnectionsOf(0, 0, 1);

            Assert.Equal(2, tuples.Count);
            Assert.True(tuples.Contains((0, 1, 1)));
            Assert.False(tuples.Contains((0, 0, 0)));
            Assert.True(tuples.Contains((0, 0, 2)));

            tuples = grid.FreeConnectionsOf(0, 1, 1);
            Assert.Equal(7, tuples.Count);
        }

        [Fact]
        public void NineMenConnectionsTest()
        {
            IGrid grid = new NineMenGrid();

            var tuples = grid.ConnectionsOf(1, 0, 1);
            Assert.Equal(4, tuples.Count);
            Assert.True(tuples.Contains((1, 0, 0)));
            Assert.True(tuples.Contains((0, 0, 1)));
            Assert.True(tuples.Contains((2, 0, 1)));
            Assert.True(tuples.Contains((1, 0, 2)));
        }

        [Fact]
        public void NineMenFreeConnectionsTest()
        {
            IGrid grid = new NineMenGrid();
            grid = grid.Apply(Move.Of(Checker.WHITE, Phase.FIRST).To(0, 0, 1));
            grid = grid.Apply(Move.Of(Checker.WHITE, Phase.FIRST).To(1, 0, 0));

            _output.WriteLine(grid.ToString());
            var tuples = grid.FreeConnectionsOf(1, 0, 1);
            Assert.Equal(2, tuples.Count);
            Assert.False(tuples.Contains((1, 0, 0)));
            Assert.False(tuples.Contains((0, 0, 1)));
            Assert.True(tuples.Contains((2, 0, 1)));
            Assert.True(tuples.Contains((1, 0, 2)));
        }
    }
}
