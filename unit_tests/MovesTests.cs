﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class MovesTests
    {
        private readonly ITestOutputHelper _output;

        public MovesTests(ITestOutputHelper output)
        {
            _output = output;
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
        }

        [Fact]
        public void TestTo()
        {
            Move m = Move.Of(Checker.WHITE, Phase.FIRST).To(0, 0, 0);

            Assert.Equal(Checker.WHITE, m.Target);
            Assert.Equal(1, m.StepsCount);
            Assert.Equal(typeof(Put), m[0].GetType());
            Assert.Equal(0, m[0].Ring);
        }

        [Fact]
        public void TestToFrom()
        {
            Move m = Move.Of(Checker.WHITE, Phase.SECOND).To(0, 0, 0).From(1, 0, 0);

            Assert.Equal(Checker.WHITE, m.Target);
            Assert.Equal(2, m.StepsCount);
            Assert.Equal(typeof(Put), m[0].GetType());
            Assert.Equal(typeof(Remove), m[1].GetType());
            Assert.Equal(0, m[0].Ring);
            Assert.Equal(1, m[1].Ring);
        }

        [Fact]
        public void TestToAndRemove()
        {
            Move m = Move.Of(Checker.WHITE, Phase.SECOND).To(0, 0, 0).AndRemove(1, 0, 0);

            Assert.Equal(Checker.WHITE, m.Target);
            Assert.Equal(2, m.StepsCount);
            Assert.Equal(typeof(Put), m[0].GetType());
            Assert.Equal(typeof(Remove), m[1].GetType());
            Assert.Equal(0, m[0].Ring);
            Assert.Equal(1, m[1].Ring);
        }

        [Fact]
        public void TestToFromAndRemove()
        {
            Move m = Move.Of(Checker.WHITE, Phase.SECOND).To(0, 0, 0).From(1, 0, 0).AndRemove(2, 1, 0);

            Assert.Equal(Checker.WHITE, m.Target);
            Assert.Equal(3, m.StepsCount);
            Assert.Equal(typeof(Put), m[0].GetType());
            Assert.Equal(typeof(Remove), m[1].GetType());
            Assert.Equal(typeof(Remove), m[2].GetType());
            Assert.Equal(0, m[0].Ring);
            Assert.Equal(1, m[1].Ring);
            Assert.Equal(2, m[2].Ring);
        }

        [Fact]
        public void TestWrongCombinations()
        {
            Move m = Move.Of(Checker.WHITE, Phase.FIRST);
            Assert.Throws<MoveException>(() => m.AndRemove(0, 0, 0));
            Assert.Throws<MoveException>(() => m.From(0, 0, 0));

            m = m.To(0, 0, 0);
            Assert.Throws<MoveException>(() => m.To(0, 0, 0));

            m = Move.Of(Checker.WHITE, Phase.SECOND).To(0, 0, 0).From(0, 0, 0);
            Assert.Throws<MoveException>(() => m.To(0, 0, 0));
            Assert.Throws<MoveException>(() => m.From(0, 0, 0));

            m = m.AndRemove(0, 0, 0);
            Assert.Throws<MoveException>(() => m.To(0, 0, 0));
            Assert.Throws<MoveException>(() => m.From(0, 0, 0));
            Assert.Throws<MoveException>(() => m.AndRemove(0, 0, 0));
        }

        [Fact]
        public void TestToString()
        {
            Move m = Move.Of(Checker.WHITE, Phase.SECOND).To(2, 0, 0).From(2, 0, 1).AndRemove(1, 2, 0);
            Assert.Equal("1-a7-d7-b2", m.ToString());

            m = Move.Of(Checker.WHITE, Phase.FIRST).To(2, 0, 0).AndRemove(1, 2, 0);
            Assert.Equal("0-a7--b2", m.ToString());
        }
    }
}
