﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.strategy;
using la_macina.it.unibo.ai.macina.model.strategy.implementations;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace unit_tests
{
    public class MattStratTests
    {
        [Fact]
        public void DebugTest()
        {
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);

            var strat = new MattStrat(8, .00001f, new la_macina.it.unibo.ai.macina.model.strategy.StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic);

            var i = 0;
            while (i < 10)
            {
                strat.ExpandNode();
                strat.ExpandNode();
                Move m = strat.GetBestMove();
                strat.SetCurrentState(strat.GetCurrentState().Evolve(m));
                i++;
            }
        }

		[Fact]
		public void LastFrontierTest()
		{
			MacinaEnvironment.CreateInstance(Checker.WHITE, 9);

			var strat = new MattStrat(1, .00001f, new la_macina.it.unibo.ai.macina.model.strategy.StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic);

			var i = 0;
			while (i < 10)
			{
				strat.ExpandNode();
				i++;
			}
		}

		[Fact]
		public void MorrisTest()
		{
			MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
            la_macina.it.unibo.ai.macina.model.strategy.StateExpander expander = new la_macina.it.unibo.ai.macina.model.strategy.StateExpander();

			//var strat = new MattStrat(8, .00001f, expander, MacinaEnvironment.GetInstance().GameMode.Heuristic);

			var oldState = State.Parse("OBBOOOOOWWBWOBWOOOOOWWOB-First-3,4-6,5");
			var newState = State.Parse("BBBOOOOOWWBWOBWOOOOOWWOB-First-3,3-6,6");

			Assert.True(expander.ClosedMorris(oldState, newState));

			var newState2 = State.Parse("OBBOOOOOWWBWOBWOOOOOWWBB-First-3,3-6,6");
			Assert.False(expander.ClosedMorris(oldState, newState2));
		}
	}
}
