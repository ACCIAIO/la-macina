using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.transposition;
using la_macina.it.unibo.ai.macina.model.transposition.symmetry;
using la_macina.it.unibo.ai.macina.model.transposition.rotation;

using System.Collections.Generic;
using System;
using Xunit;
using Xunit.Abstractions;
using System.Diagnostics;

namespace unit_tests
{
    public class TranspositionTests
    {
        private readonly ITestOutputHelper output;

        public TranspositionTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void TranspositionIstanceTest()
        {
			Transposition t = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);

			// Symmetry instances
			Transposition t0 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			Transposition t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			Transposition t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			Transposition t3 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			Transposition t4 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);

			Assert.Equal(typeof(Identity), t.GetType());
			Assert.Equal(typeof(VerticalSymmetry), t0.GetType());
			Assert.Equal(typeof(RightDiagonalSymmetry), t1.GetType());
			Assert.Equal(typeof(HorizontalSymmetry), t2.GetType());
			Assert.Equal(typeof(LeftDiagonalSymmetry), t3.GetType());
			Assert.Equal(typeof(CentralSymmetry), t4.GetType());

			// Rotation instances
			Transposition t90 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			Transposition t180 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			Transposition t270 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);

			Assert.Equal(typeof(Rotation90), t90.GetType());
			Assert.Equal(typeof(Rotation180), t180.GetType());
			Assert.Equal(typeof(Rotation270), t270.GetType());

		}

		[Fact]
        public void TraspositionApplayTest()
        {
			MacinaEnvironment.CreateInstance(Checker.WHITE, 9);

			Transposition t = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			Transposition t0 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			Transposition t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			Transposition t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			Transposition t3 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			Transposition t4 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
			Transposition t90 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			Transposition t180 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			Transposition t270 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);

			output.WriteLine(t.ToString());
			output.WriteLine(t0.ToString());
			output.WriteLine(t1.ToString());
			output.WriteLine(t2.ToString());
			output.WriteLine(t3.ToString());
			output.WriteLine(t4.ToString());
			output.WriteLine(t90.ToString());
			output.WriteLine(t180.ToString());
			output.WriteLine(t270.ToString());


			Assert.Equal((0, 0, 2),  t90.Apply(0, 0, 0));
			Assert.Equal((0, 2, 2), t180.Apply(0, 0, 0));
			Assert.Equal((0, 2, 0), t270.Apply(0, 0, 0));

			Assert.Equal((0, 0, 2), t0.Apply(0, 0, 0));
			Assert.Equal((0, 2, 2), t1.Apply(0, 0, 0));
			Assert.Equal((0, 2, 0), t2.Apply(0, 0, 0));
			Assert.Equal((0, 0, 0), t3.Apply(0, 0, 0));
			Assert.Equal((2, 0, 0), t4.Apply(0, 0, 0));
			Assert.Equal((1, 0, 0), t4.Apply(1, 0, 0));
			
		}


		[Fact]
		public void TranspositionAddTest() {
		
			// Non � un vero test, solo un controllo "veloce" sulle somme
			// Bisognerebbe eseguire le somme sullo stack e controllare anche la simmetria centrale

			#region Transposition Add

			Transposition t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			Transposition t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
            Transposition t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
            Assert.Equal(t3, t1 + t2);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
            t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            Assert.Throws<KeyNotFoundException>(() => t3 = t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
            t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            Assert.Throws<KeyNotFoundException>(() => t3 = t1 + t2);
            
            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            // Da qui in poi non aggiorniamo. Lasciamo le Assert cos�, che sono banalmente
            // vere :V

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
			t3 = t1 + t2;
            Assert.Equal(t3, t1 + t2);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //output.WriteLine("");

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

            //t1 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t2 = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            //t3 = t1 + t2;
            //output.WriteLine(t1 + " + " + t2 + " = " + t3);

			#endregion

		}
	}
}
