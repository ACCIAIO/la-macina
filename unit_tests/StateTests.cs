﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.grid;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class StateTests
    {
        private ITestOutputHelper _output;

        public StateTests(ITestOutputHelper output)
        {
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);
            _output = output;
        }

        [Fact]
        public void TestParse()
        {
            string toParse = "OBWOOOOOWWBBOBWOOOOOWWBB-First-6,6-3,3";
            State s = State.Parse(toParse);

            Assert.Equal(6, s.OwnCheckers);
            Assert.Equal(6, s.OpponentCheckers);
            Assert.Equal(Phase.FIRST, s.CurrentPhase);

            IGrid g = s.Grid;

            _output.WriteLine(g.ToString());

            Assert.Equal(Checker.BLACK, g[2, 0, 1]);
        }
    }
}
