﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.strategy;
using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace unit_tests
{
    public class StateExpanderTests
    {
        private readonly ITestOutputHelper _out;

        public StateExpanderTests(ITestOutputHelper outt)
        {
            _out = outt;
        }

        [Fact]
        public void TestExpansion()
        {
            MacinaEnvironment.CreateInstance(Checker.WHITE, 9);

            StateExpander exp = new StateExpander();
            State s = State.Parse("OOOOOOOOOOOOOOOOOOOOOOOO-First-9,9-0,0");
            var leaves = new List<State>();

            var l1 = exp.GenerateChildren(s, Checker.WHITE);

            _out.WriteLine("#################### MOSSA 1 ####################");
            for(int i = 0; i < l1.Count; i++)
            {
                _out.WriteLine("=========== " + (i+1) + " ==========\n" + l1[i]);
            }

            s = l1[0];

            var l2 = exp.GenerateChildren(s, Checker.BLACK);

            _out.WriteLine("#################### MOSSA 2 ####################");
            for (int i = 0; i < l2.Count; i++)
            {
                _out.WriteLine("=========== " + (i+1) + " ==========\n" + l2[i]);
            }

            s = l2[0];

            var l3 = exp.GenerateChildren(s, Checker.WHITE);
            _out.WriteLine("#################### MOSSA 3 ####################");
            for (int i = 0; i < l3.Count; i++)
            {
                _out.WriteLine("=========== " + (i + 1) + " ==========\n" + l3[i]);
            }

            s = l3[3];

            var l4 = exp.GenerateChildren(s, Checker.BLACK);
            _out.WriteLine("#################### MOSSA 4 ####################");
            for (int i = 0; i < l4.Count; i++)
            {
                _out.WriteLine("=========== " + (i + 1) + " ==========\n" + l4[i]);
            }

            s = l4[0];

            var l5 = exp.GenerateChildren(s, Checker.WHITE);
            _out.WriteLine("#################### MOSSA 5 ####################");
            for (int i = 0; i < l5.Count; i++)
            {
                _out.WriteLine("=========== " + (i + 1) + " ==========\n" + l5[i]);
            }

            s = l4[0];

            Assert.True(false);
        }
    }
}
