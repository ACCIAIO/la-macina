﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina
{
    enum Checker
    {
        WHITE,
        BLACK
    }

    class Environment
    {
        private static Checker _checker = Checker.WHITE;
        private static bool _set = false;

        public static Checker Checker
        {
            get
            {
                return _checker;
            }
            set {
                if (!_set)
                {
                    _checker = value;
                    _set = true;
                }
                else throw new ArgumentException("checker color already set");
            }
        }
    }
}
