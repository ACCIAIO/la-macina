﻿using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.transposition;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// La classe LimitedBackPropagatedBestFirst rappresenta una strategia di espansione
/// che fonde assieme diverse strategie di ricerca:
/// - Best first (limitata): espando in best-first tutti i nodi fino ad una certa profondità,
///   una volta raggiunto il livello massimo inizio ad espandere nodi 'meno promettenti'.
///   Quando non ho più nodi da espandere, aumento il limite e rinizio l'espansione best-first.
/// - Min-Max algorithm: durante l'espansione dei nodi viene utilizzata una variante dell'algoritmo
///   Min-Max (best-first min-max search), per la quale i valori di euristica dei figli di un nodo
///   vengono propagati verso il genitore in modo tale che il valore di euristica di un nodo generico
///   rappresenti al meglio possibile la bontà del sottoalbero che esso genera, facilitando perciò la
///   decisione della mossa da effettuare.
/// </summary>

namespace la_macina.it.unibo.ai.macina.model.strategy.implementations
{
	public class LimitedBackPropagatedBestFirst : IExpansionStrategy
	{
		#region Members

		private readonly StateExpander _expander;
		private readonly HeuristicFunction _heuristic;
		private readonly Dictionary<long, (Transposition, Node)> _hashtable = new Dictionary<long, (Transposition, Node)>();
		private readonly TranspositionStack _transpositionStack = new TranspositionStack();

		#endregion

		#region Constructors

		public LimitedBackPropagatedBestFirst(int depthLimit, StateExpander expander, HeuristicFunction heuristic)
		{
			_heuristic = heuristic;
			DepthLimit = depthLimit;
			CurrentDepthLimit = depthLimit;
			Graph = MacinaEnvironment.GetInstance().DecisionTree;

			// se c'è solo la radice, allora è un nuovo albero.
			if (Graph.NodesCount == 1)
			{
				Graph.MaxDepth = 0;
				if (MacinaEnvironment.GetInstance().Checker == Checker.WHITE)
				{
					Graph.Root = new Node(Graph.Root.State, 0, NodeType.MIN, 0);
					Graph.MinFrontier.Add(Graph.Root);
					GenerateMaxNodes = true;
				}
				else
				{
					Graph.Root = new Node(Graph.Root.State, 0, NodeType.MAX, 0);
					Graph.MaxFrontier.Add(Graph.Root);
					GenerateMaxNodes = false;
				}
				Graph.CurrentNode = Graph.Root;
			}
			_expander = expander;
		}

		#endregion

		#region Properties

		public bool GenerateMaxNodes { get; private set; }
		public int DepthLimit { get; set; }
		public int CurrentDepthLimit { get; private set; }
		public DecisionGraph Graph { get; }
		public HeuristicFunction Heuristic => _heuristic;
		public TranspositionStack GetTranspositionStack => _transpositionStack;

		#endregion

		#region Public methods

		/// <summary>
		/// Espande un nodo, ogni volta ne espande uno di un tipo diverso
		/// ... - MAX - MIN - MAX - ...
		/// </summary>
		public void ExpandNode()
		{
			//Console.WriteLine("Level: {0}", Tree.CurrentNode.Level);
			if (GenerateMaxNodes)
			{
				if (Graph.MinFrontier.Count == 0)
				{
					if (Graph.MaxFrontier.Count == 0)
					{
						// ho già aspanso tutti i nodi fino alla profondità richiesta.
						// aumento la profondità massima
						CurrentDepthLimit += DepthLimit;
						Graph.MinFrontier = Graph.LastLevelFrontier;
						Graph.LastLevelFrontier = new AutoSortingList<Node>();
					}
					else
						GenerateMaxNodes = !GenerateMaxNodes;
				}
			}
			else
			{
				if (Graph.MaxFrontier.Count == 0)
				{
					if (Graph.MinFrontier.Count == 0)
					{
						// ho già aspanso tutti i nodi fino alla profondità richiesta.
						// aumento la profondità massima
						CurrentDepthLimit += DepthLimit;
						Graph.MaxFrontier = Graph.LastLevelFrontier;
						Graph.LastLevelFrontier = new AutoSortingList<Node>();
					}
					else
						GenerateMaxNodes = !GenerateMaxNodes;
				}

			}

			if (GenerateMaxNodes)
				ExpandMIN();
			else
				ExpandMAX();

			Console.Write('.');
			// switch tra nodi di min e nodi di max
			GenerateMaxNodes = !GenerateMaxNodes;
		}

		/// <summary>
		/// Calcola lo stato a cui porta la mossa migliore.
		/// </summary>
		/// <returns>Lo stato migliore</returns>
		public Move GetBestMove()
		{
			Node bestChoiceNode = null;
			int maxHeuristic = int.MinValue;

			foreach (Node node in Graph.CurrentNode.Children)
			{
				if (maxHeuristic < node.HeuristicValue)
				{
					maxHeuristic = node.HeuristicValue;
					bestChoiceNode = node;
				}
			}
			return GetTranspositionStack.Apply(bestChoiceNode.State.LastMoveDone);
		}

		/// <summary>
		/// Imposta il nuovo stato corrente nell'albero e
		/// aggiorna il TranspositionStack di conseguenza,
		/// successivamente pulisce le frontiere dai nodi superflui.
		/// </summary>
		/// <param name="state">Il nuovo stato corrente</param>
		public void SetCurrentState(State state)
		{
            if (_hashtable.ContainsKey(state.GetHashCode()))
			{
				(Transposition t, Node node) = _hashtable[state.GetHashCode()];
				GetTranspositionStack.Add(t);
				Graph.CurrentNode = node;
foreach(Node n in Graph.CurrentNode.Children)
{
    Console.WriteLine("######################################\n" + n.State + " " + n.HeuristicValue + "\n#############################################");
}
                CleanFrontiers();
            }
			else
			{
                GetTranspositionStack.Init();
                Node n = new Node(state, Graph.CurrentNode.Level + 1,
                    (Graph.CurrentNode.NodeType == NodeType.MAX) ? NodeType.MIN : NodeType.MAX,
                    _heuristic(state));
                n.Parents.Add(Graph.CurrentNode);
                Graph.CurrentNode = n;
			    CleanFrontiers();

                if (Graph.CurrentNode.NodeType == NodeType.MAX)
                {
                    Graph.MaxFrontier.Add(Graph.CurrentNode);
                    GenerateMaxNodes = false;
                }
                else
                {
                    Graph.MinFrontier.Add(Graph.CurrentNode);
                    GenerateMaxNodes = true;
                }

                //Console.WriteLine("########################## MIN #######################");
                //foreach (Node n in Tree.MinFrontier)
                //    Console.WriteLine(n.State + "\n%%%%%%\n");
                //Console.WriteLine("########################## MAX #######################");
                //foreach (Node n in Tree.MaxFrontier)
                //    Console.WriteLine(n.State + "\n$$$$$$\n");
                //Console.WriteLine("########################## LAST #######################");
                //foreach (Node n in Tree.LastLevelFrontier)
                //    Console.WriteLine(n.State + "\n$$$$$$\n");
                //Console.WriteLine("########################## CURRENT NODE #######################");
                //Console.WriteLine(Tree.CurrentNode.State);
			}
        }

		/// <summary>
		/// Restiuisce lo stato corrente della partita
		/// </summary>
		/// <returns></returns>
		public State GetCurrentState()
		{
			return Graph.CurrentNode.State;
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Espande un nodo di massimo prelevandolo dalla MAXfrontier.
		/// Viene calcolato il minimo dell'euristica tra i nodi figli
		/// (nodi di minimo) e propagata al valore del loro nodo genitore.
		/// Viene propagato il minimo possibile perché è la scelta più
		/// probabile che l'avversario compia.
		/// </summary>
		private void ExpandMAX()
		{
			// extract the best max node and expand it into min nodes
			Node maxNode = Graph.MaxFrontier.First.Value;
			Graph.MaxFrontier.RemoveFirst();
			ExpandGeneric(maxNode, NodeType.MIN);
		}

		/// <summary>
		/// Espande un nodo di minimo prelevandolo dalla MINfrontier.
		/// Viene calcolato il massimo dell'euristica tra i nodi figli
		/// (nodi di massimo) e propagata al valore del loro nodo genitore.
		/// Viene propagato il massimo possibile perché è la scelta più
		/// probabile che il giocatore compia.
		/// </summary>
		private void ExpandMIN()
		{
			// extract the worst min node and expand it into max nodes
			Node minNode = Graph.MinFrontier.Last.Value;
			Graph.MinFrontier.RemoveLast();
			ExpandGeneric(minNode, NodeType.MAX);
		}

		private void ExpandGeneric(Node parent, NodeType childrenType)
		{
			int heuristicValue = parent.HeuristicValue;

            Checker player = parent.NodeType == NodeType.MAX ?
                 MacinaEnvironment.GetInstance().Checker.Opposite() : MacinaEnvironment.GetInstance().Checker;
            List<State> children = _expander.GenerateChildren(parent.State, player);
			foreach (State s in children)
			{
				if (!_hashtable.ContainsKey(s.GetHashCode()))
				{
					var node = new Node(s, parent.Level + 1, childrenType, _heuristic(s));
                    node.Parents.Add(parent);
					AddTraspositionsToHashTable(node);
					parent.Children.Add(node);

					if (CurrentDepthLimit == node.Level)
						Graph.LastLevelFrontier.Add(node);
					else if (NodeType.MAX == childrenType)
						Graph.MaxFrontier.Add(node);
					else
						Graph.MinFrontier.Add(node);

					if (NodeType.MAX == childrenType && heuristicValue < node.HeuristicValue ||
						NodeType.MIN == childrenType && heuristicValue > node.HeuristicValue)
						heuristicValue = node.HeuristicValue;
				}
				else
				{
					(Transposition t, Node ourChild) = _hashtable[s.GetHashCode()];
					if(!parent.Children.Contains(ourChild))
						parent.Children.Add(ourChild);
				} 


			}

			Graph.NodesCount += parent.Children.Count;

			if (parent.Level + 1 > Graph.MaxDepth)
				Graph.MaxDepth = parent.Level + 1;
			parent.UpdateHeuristicValue(heuristicValue, Graph.CurrentNode.Level);
		}

		/// <summary>
		/// Pulizia della frontiera in accordo con la scelta fatta:
		/// tutti i nodi che non sono discendenti del nodo scelto con
		/// la getBestChoice sono rimossi dalle frontiere.
		/// </summary>
		private void CleanFrontiers()
		{
			// pulizia della frontiera in accordo con la scelta fatta.
			Node current = Graph.CurrentNode;

			IEnumerable<Node> newMaxFrontier = from node in Graph.MaxFrontier
											   where node == current || node.IsDescendantOf(current)
											   select node;

			Graph.MaxFrontier = new AutoSortingList<Node>();
			foreach (Node node in newMaxFrontier)
				Graph.MaxFrontier.Add(node);

			IEnumerable<Node> newMinFrontier = from node in Graph.MinFrontier
											   where node == current || node.IsDescendantOf(current)
											   select node;

			Graph.MinFrontier = new AutoSortingList<Node>();
			foreach (Node node in newMinFrontier)
				Graph.MinFrontier.Add(node);

			IEnumerable<Node> newLastFrontier = from node in Graph.LastLevelFrontier
												where node == current || node.IsDescendantOf(current)
												select node;

			Graph.LastLevelFrontier = new AutoSortingList<Node>();
			foreach (Node node in newLastFrontier)
				Graph.LastLevelFrontier.Add(node);
        }

		/// <summary>
		/// Genera tutti gli stati trasposti di uno stato e li aggiunge alla transposition table
		/// </summary>
		/// <param name="node">Il nodo il cui stato è da trasporre</param>
		private void AddTraspositionsToHashTable(Node node)
		{
			//Console.WriteLine("Expanded node (hash: {0} - Phase: {1}):\n{2}\n", node.GetState.GetHashCode(), node.GetState.CurrentPhase, node.GetState);
			foreach (Transposition transposition in TranspositionFactory.GetAll())
			{

				State transposedState = node.State.ApplyTransposition(transposition);
				long hash = transposedState.GetHashCode();
				//Console.WriteLine("T: {0} - Hash: {1} - Phase: {2}\n{3}\n", transposition, hash, transposedState.CurrentPhase, transposedState);
				if (!_hashtable.ContainsKey(hash))
				{
					_hashtable.Add(hash, (transposition, node));
				}
			}
		}

		#endregion

	}
}
