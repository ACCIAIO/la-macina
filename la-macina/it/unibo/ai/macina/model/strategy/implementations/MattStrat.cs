﻿using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.transposition;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Raffinamento della LBPBF.
/// - Esplora l'albero in best first limitata
/// - Impiega la transposition table per ridurre il branching factor
/// - Consente di specificare una soglia di espansione per escludere nodi
///   evidentemente inutili
/// Ricordiamo che abbiamo deciso:
/// - Nodo di massimo: nodo che contiene il massimo dei figli
/// - Nodo di minimo: nodo che contiene il minimo dei figli
/// </summary>

namespace la_macina.it.unibo.ai.macina.model.strategy.implementations
{
    

    public class MattStrat : IExpansionStrategy
    {
        #region Members

        private readonly StateExpander _expander;
        private readonly HeuristicFunction _heuristic;
        /// <summary>
        /// Tabella di hashing per la gestione delle trasposizioni.
        /// long            - l'hash dello stato;
        /// Transposition   - la trasposizione da applicare allo stato
        ///                 del nodo equivalente per ottenere lo stato
        ///                 da cui si ottiene l'hash;
        /// Node            - il nodo equivalente.
        /// </summary>
        private readonly Dictionary<long, Node> _hashtable = new Dictionary<long, Node>();
        private delegate void NodeExpander();
        private NodeExpander _expand;
        private Checker _myChecker;
        private Checker _oppChecker;

        #endregion

        #region Properties
        
        public int DepthLimit { get; private set; }
        public float Threshold { get; private set; }
        public int CurrentDepthLimit { get; private set; }
        public DecisionGraph Graph { get; private set; }
        public HeuristicFunction Heuristic => _heuristic;
        public TranspositionStack TranspositionStack { get; private set; }

        #endregion

        #region Constructors

        public MattStrat(int depthLimit,
            float exclusionThreshold,
            StateExpander expander,
            HeuristicFunction heuristic)
        {
            if (0f >= exclusionThreshold)
                throw new ArgumentException("The threshold cannot be <= 0");
            _myChecker = MacinaEnvironment.GetInstance().Checker;
            _oppChecker = _myChecker.Opposite();
            _heuristic = heuristic;
            _expander = expander;
            DepthLimit = depthLimit;
            Threshold = exclusionThreshold;
            CurrentDepthLimit = depthLimit;
			TranspositionStack = new TranspositionStack();

            if (_myChecker == Checker.WHITE)
                Graph = new DecisionGraph(new Node(new State(), NodeType.MAX, int.MinValue));
            // Se sono B, inizio scegliendo il minimo tra i nodi di primo livello.
            else
                Graph = new DecisionGraph(new Node(new State(), NodeType.MIN, int.MaxValue));
            ConfigExpansion();
        }

        #endregion
        
        #region Public methods

        /// <summary>
        /// Espande un nodo.
        /// Prova ad espandere per primo il tipo di nodo segnalato in Expand.
        /// Se impossibilitato, prova ad espandere l'altro tipo di nodo.
        /// Se entrambe le frontiere sono vuote, prova dalla frontiera dei
        /// nodi di ultimo livello.
        /// Se tutte sono vuote, l'albero è stato espanso completamente.
        /// </summary>
        public void ExpandNode() 
        {
            bool canExpandMin = Graph.Frontiers[NodeType.MIN].Count != 0;
            bool canExpandMax = Graph.Frontiers[NodeType.MAX].Count != 0;
            bool lastLevelFrontierIsEmpty = Graph.LastLevelFrontier.Count == 0;

            // Fase1 : Determina l'espansione da effettuare
            // Se vuoi espandere un nodo di minimo, ma hai solo nodi di massimo
            if (_expand == ExpandMIN && !canExpandMin && canExpandMax)
                // Allora prova ad espandere un nodo di massimo
                _expand = ExpandMAX;
            // Se vuoi espandere un nodo di massimo, ma hai solo nodi di minimo
            else if (_expand == ExpandMAX && !canExpandMax && canExpandMin)
                // Allora prova ad espandere un nodo di minimo
                _expand = ExpandMIN;
            // Se hai completato la ricerca fino alla profondità massima
            else if (!canExpandMax && !canExpandMin && !lastLevelFrontierIsEmpty)
            {
                // Incrementa il limite di profondità
                CurrentDepthLimit += DepthLimit;
                // Sposta la frontiera di ultimo livello nella frontiera appropriata
                // e determina quale espansione effettuare
                if (Graph.LastLevelFrontier.First.Value.NodeType == NodeType.MAX)
                {
                    Graph.Frontiers[NodeType.MAX] = Graph.LastLevelFrontier;
                    _expand = ExpandMAX;
                }
                else
                {
                    Graph.Frontiers[NodeType.MIN] = Graph.LastLevelFrontier;
                    _expand = ExpandMIN;
                }
                Graph.LastLevelFrontier = new AutoSortingList<Node>();
            }
            else if(!canExpandMax && !canExpandMin && lastLevelFrontierIsEmpty)
            {
                Console.WriteLine("Espansione completa.");
				return;
            }

            // Fase 2 : Effettua l'espansione
            _expand();
            // La prossima espansione sarà del tipo di nodo opposto
            if (_expand == ExpandMIN) _expand = ExpandMAX; else _expand = ExpandMIN;

            Console.Write('.'); // DEBUG
        }

        /// <summary>
        /// Calcola lo stato a cui porta la mossa migliore.
        /// </summary>
        /// <returns>Lo stato migliore</returns>
        public Move GetBestMove()
        {
            Node bestChoice = null;
            int maxHeuristic = int.MinValue;

            if (0 == Graph.Root.Children.Count)
                Expand(Graph.Root);

            foreach (Node n in Graph.Root.Children)
            {
                if (maxHeuristic < n.HeuristicValue)
                {
                    maxHeuristic = n.HeuristicValue;
                    bestChoice = n;
                }
            }

			return bestChoice.State.SynthetizeMoveFrom(Graph.Root.State);
        }

        /// <summary>
        /// Imposta il nuovo stato corrente nell'albero e
        /// aggiorna il TranspositionStack di conseguenza,
        /// successivamente pulisce le frontiere dai nodi superflui.
        /// </summary>
        /// <param name="state">Il nuovo stato corrente</param>
        public void SetCurrentState(State state)
        {
            Node node;
            if (_hashtable.ContainsKey(state.GetHashCode()))
            {
               node = _hashtable[state.GetHashCode()];
               node.Parents.Clear();
            }
			else
			{
                node = new Node(state, Graph.Root.NodeType.Opposite(), _heuristic(state));
                _hashtable.Add(node.GetHashCode(), node);
            }
            Graph = new DecisionGraph(node);
            ConfigExpansion();
        }

        /// <summary>
        /// Restiuisce lo stato corrente della partita
        /// </summary>
        /// <returns></returns>
        public State GetCurrentState()
        {
            return Graph.Root.State;
        }

        #endregion

        #region Private methods

        private void ConfigExpansion()
        {
            // Arbitrariamente, inizio ad espandere dal tipo di current node.
            if (Graph.Root.NodeType == NodeType.MIN) _expand = ExpandMIN;
            else _expand = ExpandMAX;
        }

        /// <summary>
        /// Espande un nodo di massimo (prelevandolo dalla Tree.Frontiers[NodeType.MAX]).
        /// 
        /// 
        /// 
        /// Viene calcolato il minimo dell'euristica tra i nodi figli
        /// (nodi di minimo) e propagata al valore del loro nodo genitore.
        /// Viene propagato il minimo possibile perché è la scelta più
        /// probabile che l'avversario compia. > NON PIU' VERO
        /// </summary>
        private void ExpandMAX()
        {
            // Estrai il miglior nodo di massimo ed effettua l'espansione
            // (le Frontiere sono ordinate in ordine decrescente)
            Node node = Graph.Frontiers[NodeType.MAX].First.Value;
            Graph.Frontiers[NodeType.MAX].RemoveFirst();
            Expand(node);
        }

        /// <summary>
        /// Espande un nodo di minimo prelevandolo dalla Frontiers[NodeType.MIN].
        /// Viene calcolato il massimo dell'euristica tra i nodi figli
        /// (nodi di massimo) e propagata al valore del loro nodo genitore.
        /// Viene propagato il massimo possibile perché è la scelta più
        /// probabile che il giocatore compia.
        /// </summary>
        private void ExpandMIN()
        {
            // Estrai il miglior nodo di minimo ed effettua l'espansione
            // (le Frontiere sono ordinate in ordine decrescente)
            Node node = Graph.Frontiers[NodeType.MIN].Last.Value;
            Graph.Frontiers[NodeType.MIN].RemoveLast();
            Expand(node);
        }

        private bool ThresholdCheck(int childValue, int parentValue, NodeType parentType)
        {
			bool isInsidePlanarZone = childValue > parentValue * Threshold;
			if (NodeType.MAX == parentType)
				return isInsidePlanarZone;
            else
                return !isInsidePlanarZone;
        }

        /// <summary>
        /// Incapsula la logica di espansione di un nodo, in modo da evitare
        /// la ripetizione di codice.
        /// </summary>
        /// <param name="expandee"></param>
        /// <param name="childrenType"></param>
        private void Expand(Node expandee)
        {
            // Genero tutti gli stati ottenibili dallo stato del nodo da espandere
            Checker player = expandee.NodeType == NodeType.MAX ? _myChecker: _oppChecker;
            List<State> childStates = _expander.GenerateChildren(expandee.State,player);

            // Per ogni stato futuro
            foreach (State s in childStates)
            {
                // Se lo stato è il trasposto di uno stato già incontrato
                if (_hashtable.ContainsKey(s.GetHashCode()))
                {
                    // Allora estrai dalla hashtable il nodo equivalente
                    var node = _hashtable[s.GetHashCode()];

                    // E collegalo al nodo in espansione
                    expandee.AddChild(node);
                    node.AddParent(expandee);

                    if (CurrentDepthLimit == node.Level)
                        Graph.LastLevelFrontier.Add(node);
                    else
                        Graph.Frontiers[node.NodeType].Add(node);

                    // Riesuma le foglie del childNode se necessarie
                    //foreach (Node n in node.GetLeaves())
                    //{
                    //    if (CurrentDepthLimit == n.Level)
                    //        Graph.LastLevelFrontier.Add(n);
                    //    else
                    //        Graph.Frontiers[n.NodeType].Add(n);
                    //}
                }
                else
                {
                    // Altrimenti crea un nuovo nodo ed aggiorna la hashtable
                    var node = new Node(s, expandee.NodeType.Opposite(), _heuristic(s));
                    
                    _hashtable.Add(s.GetHashCode(), node);
                    // E collegalo al nodo in espansione
                    expandee.AddChild(node);
                    node.AddParent(expandee);

//                    if (!ThresholdCheck(node.HeuristicValue, expandee.HeuristicValue, expandee.NodeType))
//                        continue;
                                                           
                    if (CurrentDepthLimit == node.Level)
                        Graph.LastLevelFrontier.Add(node);
                    else
                        Graph.Frontiers[node.NodeType].Add(node);
                    Graph.NodesCount++;
                }
            }

            if (expandee.Level + 1 > Graph.MaxDepth)
                Graph.MaxDepth = expandee.Level + 1;
        }

        /// <summary>
        /// Pulizia della frontiera in accordo con la scelta fatta:
        /// tutti i nodi che non sono discendenti del nodo scelto con
        /// la getBestChoice sono rimossi dalle frontiere.
        /// </summary>
        private void CleanFrontiers()
        {
            // pulizia della frontiera in accordo con la scelta fatta.
            Node current = Graph.Root;

			var tempList = new AutoSortingList<Node>();
            foreach (Node node in Graph.Frontiers[NodeType.MAX].ToList())
            {
                if (node == current || node.IsDescendantOf(current))
                    tempList.Add(node);
            }

            Graph.Frontiers[NodeType.MAX] = tempList;

			tempList = new AutoSortingList<Node>();
			foreach (Node node in Graph.Frontiers[NodeType.MIN].ToList())
			{
				if (node == current || node.IsDescendantOf(current))
					tempList.Add(node);
			}

			Graph.Frontiers[NodeType.MIN] = tempList;

			tempList = new AutoSortingList<Node>();
			foreach (Node node in Graph.LastLevelFrontier.ToList())
			{
				if (node == current || node.IsDescendantOf(current))
					tempList.Add(node);
			}

			Graph.LastLevelFrontier = tempList;
        }

        /// <summary>
        /// Genera tutti gli stati trasposti di uno stato e li aggiunge alla transposition table
        /// </summary>
        /// <param name="node">Il nodo il cui stato è da trasporre</param>
        private long AddTraspositionsToHashTable(Node node)
        {
            long res = 0;
            foreach (Transposition transposition in TranspositionFactory.GetAll())
            {

                State transposedState = node.State.ApplyTransposition(transposition);
                long hash = transposedState.GetHashCode();

                if (TranspositionEnum.IDENTITY == transposition.GetEnum())
                    res = hash;

                if (!_hashtable.ContainsKey(hash))
                {
                    _hashtable.Add(hash, node);
                }
            }
            return res;
        }

        #endregion

    }
}
