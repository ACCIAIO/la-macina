﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
    public delegate int HeuristicFunction(State state);

    interface IExpansionStrategy
    {
        DecisionGraph Graph { get; }
        void ExpandNode();
        Move GetBestMove();
        void SetCurrentState(State state);
		State GetCurrentState();
    }
}
