﻿using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.transposition;
using System;
using System.Collections.Generic;
using System.Linq;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
    public class StateExpander : IStateExpander
    {
        #region Internal methods

        /// <summary>
        /// Crea una lista di nodi figli al nodo dato aggiungendo una pedina in una posizione libera
        /// e controllando i mulini generati, nel caso rimuove una pedina avversaria.
        /// Invocato durante la prima fase di gioco.
        /// </summary>
        /// <param name="checker">Giocatore che effettua la mossa</param>
        /// <param name="node">Nodo da espandere</param>
        /// <returns>Lista di nodi figli</returns>
        private List<State> AddCheckerToTheBoard(Checker checker, State state)
        {
            List<State> successors = new List<State>();

            // per ogni posizione libera della scacchiera
            foreach ((byte ring, byte row, byte col) in state.Grid.FreePositions())
            {
                Move moveTo = Move.Of(checker, Phase.FIRST);
                // aggiungo la pedina
                moveTo = moveTo.To(ring, row, col);
                State newToState = state.Evolve(moveTo);

                // se ho chiuso un morris rimuovo una pedina avversaria
                if (ClosedMorris(state, newToState))
                    successors.AddRange(GetMorrisMoves(state, moveTo, checker));
                else
                    successors.Add(newToState);
			}
            return successors;
        }

		/// <summary>
		/// Dato uno stato e un giocatore, viene restituito vero se il giocatore ha appena chiuso un morris
		/// </summary>
		/// <param name="state">Lo stato di gioco</param>
		/// <param name="player">Il giocatore considerato</param>
		/// <returns>True se il giocatore ha appena chiuso un morris</returns>
		public bool ClosedMorris(State oldState, State newState)
		{
			Move move = newState.SynthetizeMoveFrom(oldState);
			MoveStep lastToMove = move[0];
			Checker player = move.Target;

			byte toRing = lastToMove.Ring;
			byte toRow = lastToMove.Row;
			byte toCol = lastToMove.Col;

			//Controllo i morris sulle righe
			bool morris = false;
			if (toRow != 1)//Se non sono sulla riga centrale
			{
				morris = true;
				for (byte j = 0; j < 3; j++)
				{
					morris = morris && player == newState.Grid[toRing, toRow, j];
				}
			}
			if (morris) return true;

			//Controllo i morris sulle colonne

			if (toCol != 1)//Se non sono sulla colonna centrale
			{
				morris = true;
				for (byte i = 0; i < 3; i++)
				{
					morris = morris && player == newState.Grid[toRing, i, toCol];
				}
			}
			if (morris) return true;

			//Controllo i morris sui ponti
			if (1 == toCol || 1 == toRow)//Se sono su un ponte
			{
				morris = true;
				for (byte r = 0; r < 3; r++)
				{
					morris = morris && player == newState.Grid[r, toRow, toCol];
				}
			}
			return morris;
		}

		/// <summary>
		/// Genera i nodi figli di una mossa AndRemove, ovvero dato lo stato attuale genera
		/// le mosse che rimuovono di volta in volta una pedina avversaria.
		/// </summary>
		/// <param name="parent">Nodo padre</param>
		/// <param name="partialState">Stato generato da una mossa To o FromTo</param>
		/// <param name="move">Mossa To o FromTo applicata allo stato</param>
		/// <param name="checker">Giocatore che effettua la mossa</param>
		/// <returns>Lista di nodi in cui ho rimosso una pedina avversaria</returns>
		private List<State> GetMorrisMoves(State parent, Move move, Checker checker)
        {
            List<State> successors = new List<State>();
            Checker opponentChecker = checker.Opposite();
            IEnumerable<(byte, byte, byte)> positions;
            IEnumerable<(byte, byte, byte)> morrisOpponentPositions = LineUtils.GetPositions(parent.Features.GetMorrisLines(opponentChecker));
            IEnumerable<(byte, byte, byte)> opponentCheckerPositions = parent.Grid.OccupiedPositions(opponentChecker);
            List<(byte, byte, byte)> notMorrisPositions = opponentCheckerPositions.Except(morrisOpponentPositions).ToList();

            // if esistono pedine avversarie non in un mulino rimuovo quelle
            // altrimenti rimuovo quelle dei mulini
            if (notMorrisPositions.Count > 0)
                positions = notMorrisPositions;
            else
                positions = morrisOpponentPositions;

            // per ogni pedina diversa
            foreach ((byte remove_ring, byte remove_row, byte remove_col) in positions)
            {
                Move moveToRemove = move.AndRemove(remove_ring, remove_row, remove_col);
                State newToRemoveState = parent.Evolve(moveToRemove);
                successors.Add(newToRemoveState);
            }

            return successors;
        }

        /// <summary>
        /// Crea una lista di nodi figli al nodo dato spostando una pedina in una posizione adiacente libera
        /// e controllando i mulini generati, nel caso rimuove una pedina avversaria.
        /// Invocato durante la seconda fase di gioco.
        /// </summary>
        /// <param name="checker">GIocatore che effettua la mossa</param>
        /// <param name="state">Nodo da espandere</param>
        /// <returns></returns>
        private List<State> MoveCheckerInNextPositions(Checker checker, State state)
        {
            List<State> successors = new List<State>();
            Checker opponentChecker = checker.Opposite();

            // per ogni posizione occupata dal giocatore della scacchiera
            foreach ((byte ring, byte row, byte col) in state.Grid.OccupiedPositions(checker))
            {
                
                foreach ((byte free_ring, byte free_row, byte free_col) in state.Grid.FreeConnectionsOf(ring, row, col))
                {
					Move moveFromTo = Move.Of(checker, state.CurrentPhase)
						.To(free_ring, free_row, free_col)
						.From(ring, row,col);
                    State newFromToState = state.Evolve(moveFromTo);

                    // se ho chiuso un morris rimuovo una pedina avversaria
                    if (ClosedMorris(state, newFromToState))
                        successors.AddRange(GetMorrisMoves(state, moveFromTo, checker));
                    else                   
                        successors.Add(newFromToState);
                    

                }
            }
            return successors;
        }

        /// <summary>
        /// Crea una lista di nodi figli al nodo dato spostando una pedina in una posizione qualisasi libera
        /// e controllando i mulini generati, nel caso rimuove una pedina avversaria.
        /// Invocato durante l'ultima fase di gioco.
        /// </summary>
        /// <param name="checker">Giocatore che effettua la mossa</param>
        /// <param name="node">Nodo da espandere</param>
        /// <returns>Lista di nodi figli</returns>
        private List<State> MoveCheckerInAnyPositions(Checker checker, State state)
        {
            List<State> successors = new List<State>();
            Checker opponentChecker = checker.Opposite();

            // per ogni posizione occupata dal giocatore della scacchiera
            foreach ((byte ring, byte row, byte col) in state.Grid.OccupiedPositions(checker))
            {
                Move moveFrom = Move.Of(checker, state.CurrentPhase).From(ring, row, col);
                foreach ((byte free_ring, byte free_row, byte free_col) in state.Grid.FreePositions())
                {
                    Move moveFromTo = moveFrom.To(free_ring, free_row, free_col);
                    State newFromToState = state.Evolve(moveFromTo);

                    // se ho chiuso un morris rimuovo una pedina avversaria
                    if (ClosedMorris(state, newFromToState))
                        successors.AddRange(GetMorrisMoves(state, moveFromTo, checker));
                    else
                        successors.Add(newFromToState);
                }
            }
            return successors;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Genera la lista di figli di un nodo a seconda della fase di gioco.
        /// Il metodo genera tutti e soli i figli, inclusi i trasposti.
        /// </summary>
        /// <param name="node">Stato da espandere</param>
        /// <param name="player">Giocatore che deve effettuare la mossa</param>
        /// <returns>Lista di nodi figli</returns>
        public List<State> GenerateChildren(State parent, Checker player)
        {
            List<State> nodes = null;

            switch (parent.CurrentPhase)
            {
                case Phase.FIRST:
                    nodes = AddCheckerToTheBoard(player, parent);
                    break;
                case Phase.SECOND:
                    nodes = MoveCheckerInNextPositions(player, parent);
                    break;
                case Phase.FINAL:
                    nodes = MoveCheckerInAnyPositions(player, parent);
                    break;
                default:
                    throw new InvalidOperationException("Fase di gioco del nodo non valida.");
            }
            return nodes;
        }

        #endregion
    }
}
