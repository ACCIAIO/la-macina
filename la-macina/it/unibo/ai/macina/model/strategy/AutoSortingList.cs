﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
    public class AutoSortingList<T>: LinkedList<T> where T : IComparable<T>
    {
        private Object lockThis = new object();

		/// <summary>
		/// Aggiunge un oggetto alla lista in modo ordinato
		/// utilizzando il CompareTo del tipo di oggetto.
		/// </summary>
		/// <param name="item"></param>
		public void Add(T item)
		{
            lock (lockThis)
            {
                if (null == item || Contains(item))
                    return;
			    if (Count == 0)
				    AddFirst(new LinkedListNode<T>(item));
			    else

				    for(LinkedListNode<T> node = First; node != null; node = node.Next)
				    {
     //                   Console.WriteLine(node.Value + " " + item);
					    if (item.CompareTo(node.Value) >= 0)
					    {
						    AddBefore(node, item);
						    break;
					    }
				    }
            }
		}
	}
}
