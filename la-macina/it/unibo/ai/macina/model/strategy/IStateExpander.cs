﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
    interface IStateExpander
    {
        List<State> GenerateChildren(State state, Checker player);
    }
}
