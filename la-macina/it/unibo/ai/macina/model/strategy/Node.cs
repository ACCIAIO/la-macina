﻿using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.transposition;
using System;
using System.Collections.Generic;
using System.Linq;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
	public enum NodeType : byte
	{
		MIN,
        MAX,
	}
    
    public static class Extensions
    {
        public static NodeType Opposite(this NodeType type)
        {
            return type == NodeType.MIN ? NodeType.MAX : NodeType.MIN;
        }
    }

    public class Node : IComparable<Node>
    {

        #region Members

        private List<Node> _parents = new List<Node>();
        private readonly List<Node> _children = new List<Node>();

        #endregion

        #region Constructors

        public Node(State state, NodeType nodeType, int heuristicValue)
        {
            State = state;
            NodeType = nodeType;
            HeuristicValue = heuristicValue;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Valore dell'euristica memorizzata nel nodo.
        /// Comodo per l'implementazione di alcuni algoritmi.
        /// </summary>
        public int HeuristicValue { get; private set; }
        public State State { get; }
        public int Level
        {
            get
            {
                if (0 == Parents.Count)
                    return 0;
                var l = 0;
                foreach (Node n in Parents.ToArray())
                    if (l < n.Level)
                        l = n.Level;
                return l + 1;
            }
        }
		public NodeType NodeType { get; }
		public ICollection<Node> Parents => _parents;
		public ICollection<Node> Children => _children;

		#endregion

		#region Public methods

		/// <summary>
		/// Determina se il nodo è discendente di un altro nodo.
		/// </summary>
		/// <param name="ancestor">Nodo di cui controllare se il corrente è un discendente</param>
		/// <returns>true se il nodo è un discendente di ancestor, false altrimenti</returns>
		public bool IsDescendantOf(Node ancestor)
		{
            if (ancestor.Parents.Count == 0)
                return true;
            
            // Per ogni padre
            foreach (Node parent in Parents.ToArray())
            {
                // Se il padre è discendente dell'antenato, allora
                // lo sono anche io
                if (parent.IsDescendantOf(ancestor))
                    return true;
            }
            return false;
		}

        /// <summary>
        /// Non aggiunge genitori duplicati
        /// </summary>
        /// <param name="parent"></param>
        public void AddParent(Node parent)
        {
            if (Parents.Contains(parent))
                return;
            Parents.Add(parent);
            UpdateParentHeuristicValue();
        }

        /// <summary>
        /// Non aggiunge figli duplicati
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(Node child)
        {
            if (_children.Contains(child))
                return;
            _children.Add(child);
            UpdateHeuristicValue(child.HeuristicValue);
        }

		/// <summary>
		/// Indispensabile per l'ordinamento descrescente dell'euristica nelle liste dell'albero
		/// </summary>
		/// <param name="other">Nodo con cui effettuare il confronto</param>
		/// <returns></returns>
		public int CompareTo(Node other)
		{
            if (null == other)
                return -1;
			if (HeuristicValue > other.HeuristicValue)
				return 1;
			else if (HeuristicValue < other.HeuristicValue)
				return -1;
			return 0;
		}

		public override string ToString()
		{
			return "Node: L=" + Level + ", EV=" + HeuristicValue + ", T=" + NodeType;
		}

        public List<Node> GetLeaves()
        {
            var result = new List<Node>();

            if (0 == Children.Count)
            {
                result.Add(this);
                return result;
            }

            foreach(Node child in Children)
                foreach (Node leaf in child.GetLeaves())
                    if(!result.Contains(leaf))
                        result.Add(leaf);

            return result;
        }

        new public long GetHashCode()
        {
            return State.GetHashCode();
        }

        #endregion

        #region Private methods

        /// <summary>
		/// Aggiorna il valore dell'euristica memorizzato
		/// all'interno del nodo. Comodo per l'implementazione
		/// di alcuni algoritmi di ricerca.
		/// </summary>
		/// <param name="heuristicValue">Valore con cui aggiornare l'euristica del
		/// nodo.</param>
		/// <paramref name="currentLevel"/>Livello massimo al quale propagare. Dopo le scelte
		/// sono già fisse, quindi non ha senso aggiornare.</paramref>
		private void UpdateHeuristicValue(int heuristicValue)
        {
            // I miei figli sono nodi di minimo: l'avversario sceglie quello
            // più basso: se il nuovo valore è minore del mio, allora aggiorno.
            // Viceversa per nodi di minimo.
            if (NodeType.MAX == NodeType && heuristicValue > HeuristicValue ||
                NodeType.MIN == NodeType && heuristicValue < HeuristicValue)
            {
                HeuristicValue = heuristicValue;
                UpdateParentHeuristicValue();
            }
        }

        /// <summary>
        /// Aggiorna il valore dell'euristica memorizzato
        /// all'interno del nodo parent. Generalmente
        /// invocato in seguito all'aggiornamento del valore
        /// del nodo stesso. Comodo per l'implementazione
        /// di alcuni algoritmi di ricerca.
        /// <paramref name="currentLevel"/>Livello massimo al quale propagare. Dopo le scelte
        /// sono già fisse, quindi non ha senso aggiornare.</paramref>
        /// </summary>
        private void UpdateParentHeuristicValue()
		{
            foreach(Node n in (from n in Parents select n))
                n.UpdateHeuristicValue(HeuristicValue);
		}

		#endregion

	}
}
