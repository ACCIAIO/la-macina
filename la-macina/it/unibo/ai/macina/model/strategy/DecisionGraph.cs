﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.strategy
{
    /// <summary>
    /// Grafo decisionale impiegato per la ricerca.
    /// Si tratta di un grafo aciclico, ma con percorsi
    /// infiniti.
    /// 
    /// Possibili ottimizzazioni: individuazione dei percorsi
    /// infiniti (es. spostare una pedina a destra e a sinsitra
    /// indefinitamente)
    /// </summary>
    public class DecisionGraph
    {

        private Node _root;

        /// <summary>
        /// Costruisce un grafo decisionale.
        /// </summary>
		public DecisionGraph(Node root)
		{
            Frontiers = new Dictionary<NodeType, AutoSortingList<Node>>
            {
                [NodeType.MAX] = new AutoSortingList<Node>(),
                [NodeType.MIN] = new AutoSortingList<Node>()
            };
			Root = root;
            LastLevelFrontier = new AutoSortingList<Node>();
			MaxDepth = 0;
		}

        /// <summary>
        /// Radice originale del grafo.
        /// Almost legacy.
        /// </summary>
		public Node Root {
            get
            {
                return _root;
            }
            set
            {
                if(null != value)
                    Frontiers[value.NodeType].Add(value);
                NodesCount = null == value ? 0 : 1;
                _root = value;
            }
        }

		/// <summary>
        /// Radice corrente dell'albero (impostata in
        /// seguito all'applicazione di una mossa).
        /// </summary>
		public IDictionary<NodeType, AutoSortingList<Node>> Frontiers { get; }

        /// <summary>
        /// Frontiera dei nodi sull'ultimo livello.
        /// Utile per alcuni algoritmi.
        /// </summary>
		public AutoSortingList<Node> LastLevelFrontier { get; set; }

        /// <summary>
        /// Massima profondità raggiunta nella ricerca.
        /// Essenzialmente debug.
        /// </summary>
        public int MaxDepth { get; set; }

        /// <summary>
        /// Numero di nodi espansi. Essenzialmente debug.
        /// </summary>
        public int NodesCount { get; set; }
	}
}
