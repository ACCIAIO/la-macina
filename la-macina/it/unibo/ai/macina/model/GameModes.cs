﻿using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.hashing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace la_macina.it.unibo.ai.macina.model
{
	public abstract class GameMode
	{
		#region Properties

		public ICollection<Phase> LegitPhases { get; }
		public byte Men { get; }
		public abstract IGrid EmptyGrid { get; }

		#endregion

		#region Constructor

		protected GameMode(ICollection<Phase> phases, byte men)
		{
			LegitPhases = phases;
			Men = men;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// In base alla fase di gioco corrente, controlla la validità della mossa per la modalità di gioco.
		/// </summary>
		/// <param name="phase">fase corrente di gioco</param>
		/// <param name="m">mossa da validare</param>
		/// <returns></returns>
		public virtual bool IsValidMove(Phase phase, Move m)
		{
			if (!LegitPhases.Contains(phase))
				throw new Exception("the game is in an invalid phase for the current game mode");
			if (2 <= m.StepsCount && StepTypes.FROM == m[1].Type && Phase.FIRST != phase)
				return false;
			if (2 <= m.StepsCount && StepTypes.FROM == m[1].Type)
			{
				var from = (m[1].Ring, m[1].Row, m[1].Col);
				var adjacent = EmptyGrid.ConnectionsOf(m[0].Ring, m[0].Row, m[0].Col).Contains(from);
				return Phase.FINAL == phase || adjacent;
			}
			return true;
		}

		public Phase NextPhase(byte own, byte eaten)
		{
			if (0 != own)
				return Phase.FIRST;
			if (3 == Men - eaten && LegitPhases.Contains(Phase.FINAL))
				return Phase.FINAL;
			return Phase.SECOND;
		}

		public abstract int Heuristic(State state);

		public abstract IGrid Parse(string parsable);

        public abstract IFeaturesExtractor BuildExtractor(State state);

		#endregion
	}

	public class ThreeMenMorris : GameMode
	{
		#region Properties

		public override IGrid EmptyGrid
		{
			get
			{
				return new ThreeMenGrid();
			}
		}

		#endregion

		#region Constructors

		public ThreeMenMorris() : base(new Phase[] { Phase.FIRST, Phase.SECOND }, 3) { }

		#endregion

		#region Public methods

		public override int Heuristic(State state)
		{
			throw new NotImplementedException();
		}

		public override IGrid Parse(string parsable)
		{
			throw new NotImplementedException();
		}

        public override IFeaturesExtractor BuildExtractor(State state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

	public class SixMenMorris : GameMode
	{
		#region Properties

		public override IGrid EmptyGrid
		{
			get
			{
				return new SixMenGrid();
			}
		}

		#endregion

		#region Constructors

		public SixMenMorris() : base(new Phase[] { Phase.FIRST, Phase.SECOND }, 6) { }

		#endregion

		#region Public methods

		public override int Heuristic(State state)
		{
			throw new NotImplementedException();
		}

		public override IGrid Parse(string parsable)
		{
			throw new NotImplementedException();
		}

        public override IFeaturesExtractor BuildExtractor(State state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

	public class NineMenMorris : GameMode
	{
		#region Properties

		public override IGrid EmptyGrid
		{
			get
			{
				return new NineMenGrid();
			}
		}

		#endregion

		#region Constructors

		public NineMenMorris() : base(new Phase[] { Phase.FIRST, Phase.SECOND, Phase.FINAL }, 9)
		{
		}

		#endregion

		#region Public methods

		public override int Heuristic(State state)
		{
			var currentPlayer = MacinaEnvironment.GetInstance().Checker;
			var opponent = currentPlayer.Opposite();
			return NineMenEvaluation.EvaluateForPlayer(state, currentPlayer) - NineMenEvaluation.EvaluateForPlayer(state, opponent);
		}

		public override IGrid Parse(string parsable)
		{
			Move m;
			IGrid grid = EmptyGrid;
			for (byte i = 0; i < 9; i++)
			{
				Checker checker = CheckerUtils.GetFromInitial(parsable.ElementAt(i));
				if (Checker.NONE == checker || Checker.INVALID == checker)
					continue;
				m = Move.Of(checker, Phase.FIRST).To((byte)(2 - i / 3), 0, (byte)(i % 3));
				grid = grid.Apply(m);
			}

			for (byte i = 0; i < 6; i++)
			{
				Checker checker = CheckerUtils.GetFromInitial(parsable.ElementAt(9 + i));
				if (Checker.NONE == checker || Checker.INVALID == checker)
					continue;
				var ring = 3 > i ? 2 - i % 3 : i % 3;
				m = Move.Of(checker, Phase.FIRST).To((byte)ring, 1, (byte)(3 > i ? 0 : 2));
				grid = grid.Apply(m);
			}

			for (byte i = 0; i < 9; i++)
			{
				Checker checker = CheckerUtils.GetFromInitial(parsable.ElementAt(15 + i));
				if (Checker.NONE == checker || Checker.INVALID == checker)
					continue;
				m = Move.Of(checker, Phase.FIRST).To((byte)(i / 3), 2, (byte)(i % 3));
				grid = grid.Apply(m);
			}

			return grid;
		}

        public override IFeaturesExtractor BuildExtractor(State state)
        {
            return new NineMenFeaturesExtractor(state);
        }

        #endregion
    }

	public class TwelveMenMorris : GameMode
	{
		#region Properties

		public override IGrid EmptyGrid
		{
			get
			{
				return new TwelveMenGrid();
			}
		}

		#endregion

		#region Constructors

		public TwelveMenMorris() : base(new Phase[] { Phase.FIRST, Phase.SECOND, Phase.FINAL }, 12) { }

		#endregion

		#region Public methods

		public override int Heuristic(State state)
		{
			throw new NotImplementedException();
		}

		public override IGrid Parse(string parsable)
		{
			throw new NotImplementedException();
		}

        public override IFeaturesExtractor BuildExtractor(State state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
