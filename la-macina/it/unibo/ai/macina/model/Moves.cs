﻿using la_macina.it.unibo.ai.macina.model.grid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model
{
    /// <summary>
    /// Contiene le traslazioni dalla nostra rappresentazione a quella del server e viceversa
    /// </summary>
    public class MoveStepTranslations
    {
        private static readonly (byte, char) _externalCenter = (4, 'd');
        private static readonly Dictionary<(byte, byte, byte), (byte, char)> _internalToExternal =
            new Dictionary<(byte, byte, byte), (byte, char)>
            {
                { (0, 0, 0), (5, 'c') },
                { (0, 0, 1), (5, 'd') },
                { (0, 0, 2), (5, 'e') },
                { (0, 1, 0), (4, 'c') },
                { (0, 1, 2), (4, 'e') },
                { (0, 2, 0), (3, 'c') },
                { (0, 2, 1), (3, 'd') },
                { (0, 2, 2), (3, 'e') },
                { (1, 0, 0), (6, 'b') },
                { (1, 0, 1), (6, 'd') },
                { (1, 0, 2), (6, 'f') },
                { (1, 1, 0), (4, 'b') },
                { (1, 1, 2), (4, 'f') },
                { (1, 2, 0), (2, 'b') },
                { (1, 2, 1), (2, 'd') },
                { (1, 2, 2), (2, 'f') },
                { (2, 0, 0), (7, 'a') },
                { (2, 0, 1), (7, 'd') },
                { (2, 0, 2), (7, 'g') },
                { (2, 1, 0), (4, 'a') },
                { (2, 1, 2), (4, 'g') },
                { (2, 2, 0), (1, 'a') },
                { (2, 2, 1), (1, 'd') },
                { (2, 2, 2), (1, 'g') },
            };

        public static (byte, char) TranslateToExternal(byte ring, byte row, byte col)
        {
            return _internalToExternal.GetValueOrDefault((ring, row, col), _externalCenter);
        }
    }

    public enum StepTypes
    {
        TO,
        FROM,
        ANDREMOVE
    }

    /// <summary>
    /// Interfaccia rappresentante una singola step
    /// (Una mossa si compone di una o più step)
    /// </summary>
    public abstract class MoveStep
    {
        #region Properties

        /// <summary>
        /// Rappresentazione dello step come short
        /// </summary>
        public ushort BitWise { get; }
        /// <summary>
        /// Ring su cui è effettuata lo step
        /// </summary>
        public byte Ring { get; }
        /// <summary>
        /// La riga del target dello step
        /// </summary>
        public byte Row { get; }
        /// <summary>
        /// La colonna del target dello step
        /// </summary>
        public byte Col { get; }
        /// <summary>
        /// Se lo step riguarda la posizione centrale di un ring
        /// </summary>
        public bool IsMiddle { get; }
        /// <summary>
        /// Identifica il tipo di step
        /// </summary>
        public StepTypes Type { get; }

        #endregion

        #region Constructor

        protected MoveStep(StepTypes type, bool isMiddle, ushort bitWise, byte ring, byte row, byte col)
        {
            Type = type;
            IsMiddle = isMiddle;
            BitWise = bitWise;
            Ring = ring;
            Row = row;
            Col = col;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Applica questa mossa al ring passato. Solo Ring
        /// deve poter accedere alla propria rappresentazione
        /// come short, aka solo Ring usa questo metodo
        /// </summary>
        /// <param name="us">Stato del ring come short.</param>
        /// <returns>Stato del ring dopo l'applicazione della mossa (come short)</returns>
        public abstract ushort ApplyBitOperation(ushort us);

        public override string ToString()
        {
            var tuple = MoveStepTranslations.TranslateToExternal(Ring, Row, Col);
            return "" + tuple.Item2 + tuple.Item1;
        }

        #endregion
    }
    
    /// <summary>
    /// Parte di mossa in cui viene posizionata una pedina sulla griglia
    /// </summary>
    public class Put : MoveStep
    {

        public Put(Checker state, byte ring, byte x, byte y) : 
            base(
                StepTypes.TO,
                4 == x * 3 + y,
                (ushort)((byte)state << (4 == x * 3 + y ? 0 : 2 * (x * 3 + y - (4 > x * 3 + y ? 0 : 1)))),
                ring,
                x,
                y
                )
        { }

        public override ushort ApplyBitOperation(ushort us)
        {
            return (ushort)(us | BitWise);
        }
    }
    
    /// <summary>
    /// Parte di mossa in cui viene rimossa una pedina dalla griglia
    /// </summary>
    public class Remove : MoveStep
    {

        public Remove(StepTypes type, byte ring, byte x, byte y) :
            base(
                type, 
                4 == x * 3 + y,
                4 == x * 3 + y ? (ushort)0 : (ushort)~(3 << 2 * (x * 3 + y - (4 > x * 3 + y ? 0 : 1))),
                ring,
                x,
                y
                )
        { }

        public override ushort ApplyBitOperation(ushort us)
        {
            return (ushort)(us & BitWise);
        }
    }

    /// <summary>
    /// Mossa completa, che si compone di al massimo 3 IMoveStep.
    /// </summary>
    public class Move : IEnumerable<MoveStep>
    {
        #region Static      

        /// <summary>
        /// Crea una mossa di base in cui si indica il solo colore della pedina interessata, senza specificarne alcuno movimento.
        /// </summary>
        /// <param name="target">Il colore della pedina oggetto della mossa</param>
        /// <returns></returns>
        public static Move Of(Checker target, Phase phase)
        {
            return new Move(target, phase);
        }

        #endregion

        #region Properties & indexers

        public virtual MoveStep this[byte index] => throw new IndexOutOfRangeException("index out of range: " + index);

        public Phase Phase { get; }

        public Checker Target;
        public virtual int StepsCount => 0;

        #endregion

        #region Constructors

        protected Move(Checker target, Phase phase)
        {
            Target = target;
            Phase = phase;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Primo passaggio per la definizione di una mossa: identifica dove la pedina verrà posta.
        /// </summary>
        /// <param name="ring">il ring della destinazione</param>
        /// <param name="row">la riga della destinazione</param>
        /// <param name="col">la colonna della destinazione</param>
        /// <returns></returns>
        public virtual Move To(byte ring, byte row, byte col)
        {
            IList<MoveStep> steps = new List<MoveStep>
            {
                new Put(Target, ring, row, col)
            };
            return new ToMove(Target, Phase, steps);
        }

        /// <summary>
        /// Da utilizzare in cascata a To() per identificare un movimento di una pedina già giocata.
        /// </summary>
        /// <param name="ring">il ring della posizione originale</param>
        /// <param name="row">la riga della posizione originale</param>
        /// <param name="col">la colonna della posizione originale</param>
        /// <returns></returns>
        public virtual Move From(byte ring, byte row, byte col)
        {
            throw new MoveException("va specificata una destinazione della pedina prima");
        }

        /// <summary>
        /// Da usare in coda a To() o a From(), identifica una mossa che, a causa della creazione di un mulino come conseguenza, cancella una pedina avversaria.
        /// </summary>
        /// <param name="ring">ring della pedina da rimuovere dal gioco</param>
        /// <param name="row">riga della pedina da rimuovere dal gioco</param>
        /// <param name="col">colonna della pedina da rimuovere dal gioco</param>
        /// <returns></returns>
        public virtual Move AndRemove(byte ring, byte row, byte col)
        {
            throw new MoveException("non si può rimuovere una pedina senza aver eseguito una mossa");
        }

        public virtual IEnumerator<MoveStep> GetEnumerator()
        {
            return Enumerable.Empty<MoveStep>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Codifica la mossa come <to>-<from>-<remove>.
        /// Se from o remove non ci sono, sono stringhe vuote.
        /// </summary>
        /// <returns>La mossa codificata</returns>
        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.Append((byte)Phase);
            b.Append('-');
            b.Append(this[0].ToString());
            b.Append('-');
            if (2 <= StepsCount && Phase.SECOND == Phase)
                b.Append(this[1].ToString());
            b.Append('-');
            if (3 == StepsCount)
                b.Append(this[2].ToString());
            else if (2 == StepsCount && Phase.FIRST == Phase)
                b.Append(this[1].ToString());
            return b.ToString();

        }

        #endregion

        #region Classes

        protected class ToMove : Move
        {
            #region Members

            protected readonly IList<MoveStep> _steps;

            #endregion

            #region Properties & indexers

            public override MoveStep this[byte index] => _steps[index];
            public override int StepsCount => _steps.Count;

            #endregion

            #region Constructors

            public ToMove(Checker target, Phase phase, IList<MoveStep> steps) : base(target, phase)
            {
                _steps = steps;
                Target = target;
            }

            #endregion

            #region Public methods

            public override Move To(byte ring, byte row, byte col)
            {
                throw new MoveException("non si possono specificaare più destinazioni per la stessa pedina");
            }

            public override Move From(byte ring, byte row, byte col)
            {
                IList<MoveStep> steps = _steps.Select(s => s).ToList();
                steps.Add(new Remove(StepTypes.FROM, ring, row, col));
                return new ToFromMove(Target, Phase, steps);
            }

            public override Move AndRemove(byte ring, byte row, byte col)
            {
                IList<MoveStep> steps = _steps.Select(s => s).ToList();
                steps.Add(new Remove(StepTypes.ANDREMOVE, ring, row, col));
                return new ToAndRemove(Target, Phase, steps);
            }

            public override IEnumerator<MoveStep> GetEnumerator()
            {
                return _steps.GetEnumerator();
            }

            #endregion
        }

        protected class ToAndRemove : ToMove
        {
            #region Constructors

            public ToAndRemove(Checker target, Phase phase, IList<MoveStep> steps) : base(target, phase, steps) { }

            #endregion

            #region Public methods

            public override Move From(byte ring, byte row, byte col)
            {
                throw new MoveException("non puoi specificare un movimento a mossa conclusa");
            }

            public override Move AndRemove(byte ring, byte row, byte col)
            {
                throw new MoveException("non si possono rimuovere più pedine come risultato di una mossa");
            }

            public override IEnumerator<MoveStep> GetEnumerator()
            {
                return _steps.GetEnumerator();
            }

            #endregion
        }

        protected class ToFromMove : ToMove
        {
            #region Constructors

            public ToFromMove(Checker target, Phase phase, IList<MoveStep> steps) : base(target, phase, steps)
            {
            }

            #endregion

            #region Public methods

            public override Move From(byte ring, byte row, byte col)
            {
                throw new MoveException("non si possono specificare più posizioni iniziali per la stessa pedina");
            }

            public override Move AndRemove(byte ring, byte row, byte col)
            {
                IList<MoveStep> steps = _steps.Select(s => s).ToList();
                steps.Add(new Remove(StepTypes.ANDREMOVE, ring, row, col));
                return new ToFromAndRemove(Target, Phase, steps);
            }

            #endregion
        }

        protected class ToFromAndRemove : ToFromMove
        {
            #region Constructors

            public ToFromAndRemove(Checker target, Phase phase, IList<MoveStep> steps) : base(target, phase, steps) { }

            #endregion

            #region Public methods

            public override Move AndRemove(byte ring, byte row, byte col)
            {
                throw new MoveException("non si possono rimuovere più pedine come risultato di una mossa");
            }

            #endregion
        }

        #endregion
    }

}
