﻿using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.hashing;
using la_macina.it.unibo.ai.macina.model.transposition;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace la_macina.it.unibo.ai.macina.model
{
    /// <summary>
    /// Rappresenta le fasi di gioco: inserimento di pedine (PHASE1), movimento sulla griglia (PHASE2), volo (FLYING)
    /// </summary>
    public enum Phase : byte
    {
        FIRST,
        SECOND,
        FINAL
    }

    /// <summary>
    /// Rappresentazione dello stato di gioco
    /// </summary>
    public class State
    {
        #region Static

        public static State Parse(string parsable)
        {
            var env = MacinaEnvironment.GetInstance();
            var tokens = parsable.Split("-", StringSplitOptions.RemoveEmptyEntries);
            var hands = tokens[2].Split(",", StringSplitOptions.RemoveEmptyEntries);
            var gridNs = tokens[3].Split(",", StringSplitOptions.RemoveEmptyEntries);
            var grid = env.GameMode.Parse(tokens[0]);
            var phase = Enum.Parse<Phase>(tokens[1].ToUpper());
            var white = Convert.ToByte(hands[0]);
            var black = Convert.ToByte(hands[1]);
            var whiteEaten = (byte)(env.Men - white - Convert.ToByte(gridNs[0]));
            var blackEaten = (byte)(env.Men - black - Convert.ToByte(gridNs[1]));

            var amIWhite = Checker.WHITE == env.Checker;

            return new State(
                grid,
                phase,
                amIWhite ? white : black,
                amIWhite ? black : white,
                amIWhite ? whiteEaten : blackEaten,
                amIWhite ? blackEaten : whiteEaten
                );
        }

        #endregion

        #region Properties

        /// <summary>
        /// Numero di pedine ancora in mano al giocatore
        /// </summary>
        public byte OwnCheckers { get; }

        /// <summary>
        /// Numero di pedine ancora in mano all'avversario
        /// </summary>
        public byte OpponentCheckers { get; }

        /// <summary>
        /// Numero di pedine del giocatore mangiate dall'avversario
        /// </summary>
        public byte OwnEatenCheckers { get; }

        /// <summary>
        /// Numero di pedine dell'avversario mangiate dal giocatore
        /// </summary>
        public byte OpponentEatenCheckers { get; }

        /// <summary>
        /// Fase corrente di gioco. Con lo svolgersi delle azioni, varia secondo quanto stabilito dalla modalità di gioco.
        /// </summary>
        public Phase CurrentPhase { get; }

        public IFeaturesExtractor Features { get; }

		/// <summary>
        /// Stato della griglia corrente
        /// </summary>
        public IGrid Grid { get; }

        #endregion

        #region Constructors

		/// <summary>
		/// Crea una griglia vuota estrapolando le informazioni sulla
		/// modalità di gioco dall'Environment.
		/// </summary>
		public State() : this(
            MacinaEnvironment.GetInstance().GameMode.EmptyGrid,
            Phase.FIRST,
            MacinaEnvironment.GetInstance().Men, 
            MacinaEnvironment.GetInstance().Men,
            0,
            0
            ) { }

		/// <summary>
		/// Crea uno stato identico a quello passato ma con una griglia diversa
		/// Utilizzato per compiere le trasposizioni della griglia
		/// </summary>
		/// <param name="grid"></param>
		/// <param name="state"></param>
		private State(IGrid grid, State state) : 
            this(
                grid,
                state.CurrentPhase, 
                state.OwnCheckers, 
                state.OpponentCheckers, 
                state.OwnEatenCheckers, 
                state.OpponentEatenCheckers
                )
		{

		}

        /// <summary>
        /// Crea un nuovo stato a partire da una grglia di gioco. 
        /// </summary>
        /// <param name="grid">Griglia di gioco</param>
        /// <param name="own">Numero di pedine a disposizione del giocatore</param>
        /// <param name="opponent">Numero di pedine a disposizione dell'avversario</param>
        private State(IGrid grid, Phase phase, byte own, byte opponent, byte ownEaten, byte opponentEaten)
        {
            OwnEatenCheckers = ownEaten;
            OpponentEatenCheckers = opponentEaten;

            CurrentPhase = phase;
            Grid = grid;
            OwnCheckers = own;
            OpponentCheckers = opponent;

            Features = MacinaEnvironment.GetInstance().GameMode.BuildExtractor(this);
        }

        #endregion

        #region Internal methods

        private bool CheckBlockCondition()
        {
            var opponent = MacinaEnvironment.GetInstance().Checker.Opposite();
            for (byte r = 0; r < Grid.RingCount; r++)
            {
                for(byte row = 0; row < 3; row++)
                {
                    for(byte col = 0; col < 3; col++)
                    {
                        if (opponent == Grid[r, row, col] && 0 == Grid.FreeConnectionsOf(r, row, col).Count)
                            return false;
                    }
                }
            }
            return true;
        }

        private IList<(byte, byte, byte, Checker, Checker)> GetChanges(State old)
        {
            var changes = new List<(byte, byte, byte, Checker, Checker)>();

            for (byte r = 0; r < Grid.RingCount; r++)
                for (byte row = 0; row < 3; row++)
                    for (byte col = 0; col < 3; col++)
                    {
                        var oldChecker = old.Grid[r, row, col];
                        var newChecker = Grid[r, row, col];
                        if (Checker.INVALID == oldChecker || Checker.INVALID == newChecker)
                            continue;
                        if (oldChecker != newChecker)
                            changes.Add((r, row, col, oldChecker, newChecker));
                    }
            return changes;
        }

        private Move SynthetizeFirstPhaseMove(State old)
        {
			
			Move m;
            Checker target;
            var env = MacinaEnvironment.GetInstance();
            var changes = GetChanges(old);

            var to = (from change in changes where Checker.NONE == change.Item4 && env.Checker == change.Item5 select change);
            if (1 != to.Count())
                to = (from change in changes where Checker.NONE == change.Item4 && env.Checker.Opposite() == change.Item5 select change);
            target = to.ElementAt(0).Item5;

            m = Move.Of(target, old.CurrentPhase).To(to.ElementAt(0).Item1, to.ElementAt(0).Item2, to.ElementAt(0).Item3);

            var rem = from change in changes where target.Opposite() == change.Item4 && Checker.NONE == change.Item5 select change;

            if (1 == rem.Count())
            {
                var r = rem.ElementAt(0);
                m = m.AndRemove(r.Item1, r.Item2, r.Item3);
            }

            return m;
        }

        private Move SynthetizeSecondPhaseMove(State old)
        {
            Move m;
            Checker target;
            var env = MacinaEnvironment.GetInstance();
            var changes = GetChanges(old);

            var to = (from change in changes where Checker.NONE == change.Item4 && env.Checker == change.Item5 select change);
            if (1 != to.Count())
                to = (from change in changes where Checker.NONE == change.Item4 && env.Checker.Opposite() == change.Item5 select change);
            target = to.ElementAt(0).Item5;

            var from = (from change in changes where target == change.Item4 && Checker.NONE == change.Item5 select change).Single();

            m = Move.Of(target, old.CurrentPhase).To(to.ElementAt(0).Item1, to.ElementAt(0).Item2, to.ElementAt(0).Item3);
            m = m.From(from.Item1, from.Item2, from.Item3);

            var rem = from change in changes where target.Opposite() == change.Item4 && Checker.NONE == change.Item5 select change;
            if (1 == rem.Count())
            {
                var r = rem.ElementAt(0);
                m = m.AndRemove(r.Item1, r.Item2, r.Item3);
            }
            return m;
        }

        #endregion

        #region Public Methods

        public bool IsGoal()
        {
            var env = MacinaEnvironment.GetInstance();
            var eatWin = 2 == env.Men - OpponentEatenCheckers;
            var blockWin = CheckBlockCondition();

            return eatWin || blockWin;
        }

        public State ApplyTransposition(Transposition t)
        {
            var grid = MacinaEnvironment.GetInstance().GameMode.EmptyGrid;
            foreach ((byte, byte, byte) conn in Grid.OccupiedPositions())
            {
                var m = Move.Of(Grid[conn.Item1, conn.Item2, conn.Item3], Phase.FIRST);
                var transposed = t.Apply(conn.Item1, conn.Item2, conn.Item3);
                m = m.To(transposed.Item1, transposed.Item2, transposed.Item3);
                grid = grid.Apply(m); // non esegue la apply perchè m.StepCount = 0
            }
            return new State(grid, this);
        }

        /// <summary>
        /// Ritorna una nuova istanza dello stato di gioco risultato
        /// dell'applicazione della mossa
        /// </summary>
        /// <param name="move">La mossa da eseguire</param>
        /// <returns>Una nuova istanza rappresentante il nuovo stato del gioco</returns>
        public State Evolve(Move move)
        {
            var own = OwnCheckers;
            var opponent = OpponentCheckers;
            var ownEaten = OwnEatenCheckers;
            var opponentEaten = OpponentEatenCheckers;
            var env = MacinaEnvironment.GetInstance();
            if(1 == move.StepsCount)
            {
                own -= (byte)(move.Target == env.Checker ? 1 : 0);
                opponent -= (byte)(move.Target == env.Checker ? 0 : 1);
            }
            if(3 == move.StepsCount)
            {
                ownEaten += (byte)(move.Target == env.Checker ? 0 : 1);
                opponentEaten += (byte)(move.Target == env.Checker ? 1 : 0);
            }

            return new State(
                null == move ? Grid : Grid.Apply(move),
                env.GameMode.NextPhase(own, ownEaten),
                own,
                opponent,
                ownEaten,
                opponentEaten
                );
        }

        public Move SynthetizeMoveFrom(State old)
        {
            if (Phase.FIRST == old.CurrentPhase)
                return SynthetizeFirstPhaseMove(old);
            return SynthetizeSecondPhaseMove(old);
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine(Grid.ToString());
            return b.ToString();
        }

        new public long GetHashCode()
        {
            return MacinaEnvironment.GetInstance().HashFactory.GetHashingFunction(HashingFunction.POWER).Hash(this);
        }

        #endregion
    }
}
