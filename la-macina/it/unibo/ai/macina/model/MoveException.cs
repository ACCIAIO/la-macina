﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model
{
    public class MoveException : Exception
    {
        public MoveException() : base() { }
        public MoveException(string msg) : base(msg) { }
    }
}
