﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    public class NineMenGrid : ThreeRingsGrid
    {
        #region Constructors

        public NineMenGrid() : base()
        {
        }

        protected NineMenGrid(NineMenGrid board, Move move) : base(board, move)
        {
        }

        #endregion

        #region Internal methods

        protected virtual bool IsEdge(byte x, byte y)
        {
            return (0 == x || 2 == x) && (0 == y || 2 == y);
        }

        #endregion

        #region Public methods

        public override ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.ConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y) && !IsEdge(x, y))
            {
                if(0 <= ring - 1)
                    result.Add(((byte)(ring - 1), x, y));
                if(2 >= ring + 1)
                    result.Add(((byte)(ring + 1), x, y));
            }
            return result;
        }

        public override ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.FreeConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y) && !IsEdge(x, y))
            {
                if (0 <= ring - 1 && Checker.NONE == this[(byte)(ring - 1), x, y])
                    result.Add(((byte)(ring - 1), x, y));

                if (2 >= ring + 1 && Checker.NONE == this[(byte)(ring + 1), x, y])
                    result.Add(((byte)(ring + 1), x, y));

            }
            return result;
        }

        public override IGrid Apply(Move move)
        {
            return new NineMenGrid(this, move);
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.Append(base.ToString());

            b.AppendLine();
            return b.ToString();
        }

        #endregion
    }
}
