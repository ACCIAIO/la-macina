﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    /// <summary>
    /// Griglia per la variante a 3 (Three Men's Morris). Ha un solo anello.
    /// </summary>
    public class ThreeMenGrid : OneRingGrid
    {
        #region Members

        private byte _center;

        #endregion

        #region Indexers

        public override Checker this[byte ring, byte x, byte y]
        {
            get
            {
                CheckRing(ring);
                var tmp = base[ring, x, y];
                return 4 == x * 3 + y ? (Checker)Enum.ToObject(typeof(Checker), _center) : tmp;
            }
        }

        #endregion

        #region Constructors

        public ThreeMenGrid() : base()
        {
        }

        private ThreeMenGrid(ThreeMenGrid board, Move move) : base(board, move)
        {
            foreach (MoveStep s in move)
                if (s.IsMiddle)
                    if (s.GetType() == typeof(Put))
                        _center = (byte)s.BitWise;
                    else
                        _center = (byte)Checker.NONE;
        }

        #endregion

        #region Public methods

        public override ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.ConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y))
                result.Add((ring, 1, 1));
            if (!IsNotCenter(x, y))
                for (byte row = 0; row < 3; row++)
                    for (byte col = 0; col < 3; col++)
                        if (IsNotCenter(row, col))
                            result.Add((0, row, col));
            return result;
        }

        public override ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.FreeConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y) && Checker.NONE == this[0, 1, 1])
                result.Add((ring, 1, 1));
            if(!IsNotCenter(x, y))
                for (byte row = 0; row < 3; row++)
                    for(byte col = 0; col < 3; col++)
                        if (IsNotCenter(row, col) && Checker.NONE == this[0, row, col])
                            result.Add((0, row, col));
            return result;
        }

        public override ICollection<(byte, byte, byte)> OccupiedPositions()
        {
            var result = base.OccupiedPositions();

            if ((byte)Checker.NONE != _center)
                result.Add((0, 1, 1));

            return result;
        }

		public override ICollection<(byte, byte, byte)> OccupiedPositions(Checker checker)
		{
			var result = base.OccupiedPositions(checker);

			if ((byte)checker == _center)
				result.Add((0, 1, 1));

			return result;
		}

		public override ICollection<(byte, byte, byte)> FreePositions()
        {
            var result = base.OccupiedPositions();

            if ((byte)Checker.NONE == _center)
                result.Add((0, 1, 1));

            return result;
        }

        public override ICollection<(byte, byte, byte)> AllPositions()
        {
            var result = base.AllPositions();

            result.Add((0, 1, 1));

            return result;
        }

        public override IGrid Apply(Move move)
        {
            return new ThreeMenGrid(this, move);
        }

        #endregion
    }
}
