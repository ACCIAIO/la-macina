﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    /// <summary>
    /// Interfaccia di base per le possibili griglie di gioco (la
    /// struttura esatta dipende dalla variante)
    /// </summary>
    public interface IGrid
    {
        #region Properties

        /// <summary>
        /// Indexer per accedere al contenuto delle posizioni della griglia
        /// </summary>
        /// <param name="ring">0 ring interno</param>
        /// <param name="x">0 a sinistra</param>
        /// <param name="y">0 in alto</param>
        /// <returns>Il colore della pedina nella posizione specificata</returns>
        Checker this[byte ring, byte x, byte y] { get; }

        /// <summary>
        /// Numero di ring della griglia
        /// </summary>
        byte RingCount { get; }
        #endregion

        #region Methods

        /// <summary>
        /// Ritorna le posizioni adiacenti alla posizione specificata
        /// </summary>
        /// <param name="ring">0 ring interno</param>
        /// <param name="x">0 a sinistra</param>
        /// <param name="y">0 in alto</param>
        /// <returns>Tutte le posizioni della griglia connesse da un segmento
        /// alla posizione specificata</returns>
        ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y);

        /// <summary>
        /// Ritorna le posizioni LIBERE adiacenti alla posizione specificata
        /// </summary>
        /// <param name="ring">0 ring interno</param>
        /// <param name="x">0 a sinistra</param>
        /// <param name="y">0 in alto</param>
        /// <returns>Tutte le posizioni della griglia connesse da un segmento
        /// alla posizione specificata</returns>
        ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y);

        /// <summary>
        /// Ritorna una collezione di triple indicanti tutte le posizioni effettivamente occupate.
        /// </summary>
        /// <returns></returns>
        ICollection<(byte, byte, byte)> OccupiedPositions();

		/// <summary>
		/// Ritorna una collezione di triple indicanti tutte le posizioni occupate da un giocatore.
		/// </summary>
		/// <param name="checker">Il giocatore</param>
		/// <returns></returns>
		ICollection<(byte, byte, byte)> OccupiedPositions(Checker checker);

		/// <summary>
		/// Ritorna una collezione di triple indicanti tutte le posizioni libere.
		/// </summary>
		/// <returns></returns>
		ICollection<(byte, byte, byte)> FreePositions();

        /// <summary>
		/// Ritorna una collezione di triple indicanti tutte le posizioni.
		/// </summary>
		/// <returns></returns>
        ICollection<(byte, byte, byte)> AllPositions();

        /// <summary>
        /// Restituisce una nuova griglia risultato dell'applicazione della mossa
        /// </summary>
        /// <param name="move">La mossa da eseguire sulla griglia</param>
        /// <returns>Una nuova istanza rappresentante la nuova griglia</returns>
        IGrid Apply(Move move);

        #endregion
    }
}
