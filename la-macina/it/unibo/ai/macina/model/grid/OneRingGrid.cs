﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    /// <summary>
    /// Classe astratta alla base di tutte le griglie che prevedono un singolo anello
    /// </summary>
    public abstract class OneRingGrid : IGrid
    {
        #region Members

        private readonly Ring _ring;

        #endregion

        #region Properties & Indexers

        public virtual byte RingCount => 1;

        private Checker this[byte x, byte y]
        {
            get
            {
                return this[0, x, y];
            }
        }

        public virtual Checker this[byte ring, byte x, byte y]
        {
            get
            {
                CheckRing(ring);
                return _ring[x, y];
            }
        }

        #endregion

        #region Constructors

        protected OneRingGrid() { }

        protected OneRingGrid(OneRingGrid board, Move move)
        {
            _ring = board._ring;
            foreach (MoveStep s in move)
                if (0 == s.Ring)
                    _ring = _ring + s;

        }

        #endregion

        #region Internal methods

        protected virtual void CheckRing(byte ring)
        {
            if (0 < ring)
                throw new IndexOutOfRangeException("This board only has one ring");
        }

        protected bool IsNotCenter(byte x, byte y)
        {
            return x != y || 1 != x;
        }

        protected ICollection<(byte, byte, byte)> GetNoCheckConnectionsOf(byte ring, byte x, byte y, bool ignoreOccupied)
        {
            ICollection<(byte, byte, byte)> result = new List<(byte, byte, byte)>();

            if (IsNotCenter(x, y))
            {
                if(0 <= x - 1 && IsNotCenter((byte)(x - 1), y) && (ignoreOccupied || Checker.NONE == this[ring, (byte)(x - 1), y]))
                    result.Add((ring, (byte)(x - 1), y));
                if(2 >= x + 1 && IsNotCenter((byte)(x + 1), y) && (ignoreOccupied || Checker.NONE == this[ring, (byte)(x + 1), y]))
                    result.Add((ring, (byte)(x + 1), y));
                
                if(0 <= y - 1 && IsNotCenter(x, (byte)(y - 1)) && (ignoreOccupied || Checker.NONE == this[ring, x, (byte)(y - 1)]))
                    result.Add((ring, x, (byte)(y - 1)));
                if(2 >= y + 1 && IsNotCenter(x, (byte)(y + 1)) && (ignoreOccupied || Checker.NONE == this[ring, x, (byte)(y + 1)]))
                    result.Add((ring, x, (byte)(y + 1)));
            }

            return result;
        }

        protected List<(byte, byte, byte)> RingPositions(byte ring)
        {
            var result = new List<(byte, byte, byte)>();

            for (byte row = 0; row < 3; row++)
                for (byte col = 0; col < 3; col++)
                    if (Checker.INVALID != _ring[row, col])
                        result.Add((ring, row, col));

            return result;
        }

        #endregion

        #region Public methods

        public virtual ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y)
        {
            CheckRing(ring);
            return GetNoCheckConnectionsOf(ring, x, y, true);
        }

        public virtual ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y)
        {
            CheckRing(ring);
            return GetNoCheckConnectionsOf(ring, x, y, false);
        }

        public virtual ICollection<(byte, byte, byte)> OccupiedPositions()
        {
            var result = new List<(byte, byte, byte)>();

            for (byte row = 0; row < 3; row++)
                for (byte col = 0; col < 3; col++)
                    if (Checker.NONE != _ring[row, col] && Checker.INVALID != _ring[row, col])
                        result.Add((0, row, col));

            return result;
        }

		public virtual ICollection<(byte, byte, byte)> OccupiedPositions(Checker checker)
		{
			var result = new List<(byte, byte, byte)>();

			for (byte row = 0; row < 3; row++)
				for (byte col = 0; col < 3; col++)
					if (checker == _ring[row, col])
						result.Add((0, row, col));

			return result;
		}

		public virtual ICollection<(byte, byte, byte)> FreePositions()
        {
            var result = new List<(byte, byte, byte)>();

            for (byte row = 0; row < 3; row++)
                for (byte col = 0; col < 3; col++)
                    if (Checker.NONE == _ring[row, col] )
                        result.Add((0, row, col));

            return result;
        }

        public virtual ICollection<(byte, byte, byte)> AllPositions()
        {
            return RingPositions(0);
        }

        public abstract IGrid Apply(Move move);

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.AppendLine("*-- LEVEL 0 --*");
            b.AppendLine(_ring.ToString());
            return b.ToString();
        }

        #endregion
    }
}
