﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{

    /// <summary>
    /// Griglia per la variante a 6 uomini (6 Men's Morris). Ha due anelli concentrici con collegamenti solo su asse verticale e orizzontale.
    /// </summary>
    public class SixMenGrid : TwoRingsGrid
    {
        #region Constructors

        public SixMenGrid() : base()
        {
        }

        private SixMenGrid(SixMenGrid board, Move move) : base(board, move)
        {
        }

        #endregion

        #region Public methods

        public override ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.ConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y) && (1 == x || 1 == y))
            {
                result.Add(((byte)((ring + 1) % 2), x, y));

            }
            return result;
        }

        public override ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y)
        {
            ICollection<(byte, byte, byte)> result = base.FreeConnectionsOf(ring, x, y);
            if (IsNotCenter(x, y) && (1 == x || 1 == y) && Checker.NONE == this[(byte)((ring + 1) % 2), x, y])
            {
                result.Add(((byte)((ring + 1) % 2), x, y));

            }
            return result;
        }

        public override IGrid Apply(Move move)
        {
            return new SixMenGrid(this, move);
        }

        #endregion
    }
}
