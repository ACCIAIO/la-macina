﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    public class TwelveMenGrid : NineMenGrid
    {
        #region Constructors

        public TwelveMenGrid() : base()
        {
        }

        private TwelveMenGrid(TwelveMenGrid board, Move move) : base(board, move)
        {
        }

        #endregion

        #region Internal Methods

        protected override bool IsEdge(byte x, byte y)
        {
            return false;
        }

        #endregion

        #region Public methods

        public override IGrid Apply(Move move)
        {
            return new TwelveMenGrid(this, move);
        }

        #endregion
    }
}
