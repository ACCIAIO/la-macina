﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    /// <summary>
    /// Un ring è un singolo anello della griglia di gioco: è formato da 8 nodi in senso orario a partire da in alto a sinistra
    /// </summary>
    public struct Ring
    {
        #region Members

        private readonly ushort _state;

        #endregion

        #region Indexer

        public Checker this[byte x, byte y]
        {
            get
            {
                if (4 == x * 3 + y)
                    return Checker.INVALID;
                int shift = x * 3 + y - (4 > x * 3 + y ? 0 : 1);
                return (Checker)Enum.ToObject(typeof(Checker), (_state >> 2 * shift) & 3);
            }
        }

        #endregion

        #region Constructors

        private Ring(ushort state)
        {
            _state = state;
        }

        #endregion

        #region Public methods

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            for (byte b1 = 0; b1 < 3; b1++)
            {
                for (byte b2 = 0; b2 < 3; b2++)
                {
                    b.Append(CheckerUtils.ToString(this[b1, b2]));
                }
                b.AppendLine();
            }
            return b.ToString();
        }

        #endregion

        #region Operators overload

        public static implicit operator Ring(ushort us)
        {
            return new Ring(us);
        }

        public static Ring operator +(Ring r, MoveStep s)
        {
            return new Ring(s.ApplyBitOperation(r._state));
        }

        #endregion

    }
}
