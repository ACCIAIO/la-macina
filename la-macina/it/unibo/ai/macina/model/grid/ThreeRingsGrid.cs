﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace la_macina.it.unibo.ai.macina.model.grid
{
    public abstract class ThreeRingsGrid : TwoRingsGrid
    {
        #region Members

        private readonly Ring _ring;

        #endregion

        #region Properties & Indexers

        public override byte RingCount => 3;

        public override Checker this[byte ring, byte x, byte y]
        {
            get
            {
                CheckRing(ring);
                return 2 != ring ? base[ring, x, y] : _ring[x, y];
            }
        }

        #endregion

        #region Constructors

        public ThreeRingsGrid() : base() { }

        public ThreeRingsGrid(ThreeRingsGrid board, Move move) : base(board, move)
        {
            _ring = board._ring;
            foreach (MoveStep s in move)
                if (2 == s.Ring)
                    _ring = _ring + s;
        }

        #endregion

        #region Internal methods

        protected override void CheckRing(byte ring)
        {
            if (2 < ring)
                throw new IndexOutOfRangeException("This board only has three rings");
        }

        #endregion

        #region Public methods

        public override ICollection<(byte, byte, byte)> ConnectionsOf(byte ring, byte x, byte y)
        {
            CheckRing(ring);
            return GetNoCheckConnectionsOf(ring, x, y, true);
        }

        public override ICollection<(byte, byte, byte)> FreeConnectionsOf(byte ring, byte x, byte y)
        {
            CheckRing(ring);
            return GetNoCheckConnectionsOf(ring, x, y, false);         
        }

        public override ICollection<(byte, byte, byte)> OccupiedPositions()
        {
            var result = base.OccupiedPositions();

            for (byte row = 0; row < 3; row++)
                for (byte col = 0; col < 3; col++)
                    if (Checker.NONE != _ring[row, col] && Checker.INVALID != _ring[row, col])
                        result.Add((2, row, col));

            return result;
        }

		public override ICollection<(byte, byte, byte)> OccupiedPositions(Checker checker)
		{
			var result = base.OccupiedPositions(checker);

			for (byte row = 0; row < 3; row++)
				for (byte col = 0; col < 3; col++)
					if (checker == _ring[row, col])
						result.Add((2, row, col));

			return result;
		}

		public override ICollection<(byte, byte, byte)> FreePositions()
        {
            var result = base.FreePositions();

            for (byte row = 0; row < 3; row++)
                for (byte col = 0; col < 3; col++)
                    if (Checker.NONE == _ring[row, col])
                        result.Add((2, row, col));

            return result;
        }

        public override ICollection<(byte, byte, byte)> AllPositions()
        {
            var result = base.AllPositions();

            foreach ((byte, byte, byte) pos in RingPositions(2))
                result.Add(pos);

            return result;
        }

        public override string ToString()
        {
            return base.ToString() + "*-- LEVEL 2 --*\n" + _ring.ToString();
        }

        #endregion
    }
}
