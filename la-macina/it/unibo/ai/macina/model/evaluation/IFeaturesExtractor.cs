﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.evaluation
{
    public interface IFeaturesExtractor
    {
        ISet<int> GetMorrisLines(Checker player);
        int StuckOpponentPieces(Checker player);
        int PiecesNumber(Checker player);
        int TwoPiecesConfigNumber(Checker player);
        int ThreePiecesConfigNumber(Checker player);
        bool OpenedMorris(Checker player);
        bool DoubleMorris(Checker player);

	}
}
