﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.evaluation
{
    static class LineUtils
    {
        public struct Line
        {
            public byte RingContraint { get; }
            public byte RowConstraint { get; }
            public byte ColConstraint { get; }

            public Line(byte ringC, byte rowC, byte colC)
            {
                RingContraint = ringC;
                RowConstraint = rowC;
                ColConstraint = colC;
            }
        }

        public static readonly Line[] lines = {
            new Line(2, 0 , 255),//Prima linea orizzontale: nell'anello esterno, riga più alta
            new Line(1, 0 , 255),//Seconda linea orizzontale: nell'anello medio, riga più alta 
            new Line(0, 0 , 255),//Terza linea orizzontale: nell'anello interno, riga più alta
            new Line(255, 1 , 0), //Quarta linea orizzontale: ponte sinistro (riga 1, colonna 0)
            new Line(255, 1 , 2), //Quinta linea orizzontale: ponte destro (riga 1, colonna 2)
            new Line(0, 2 , 255),//Sesta linea orizzontale: nell'anello intero, riga più bassa
            new Line(1, 2 , 255),//Settima linea orizzontale: nell'anello medio, riga più bassa
            new Line(2, 2 , 255),//Ottava linea orizzontale: nell'anello esterno, riga più bassa

            new Line(2, 255 , 0), //Nona linea verticale: nell'anello esterno, colonna più a sinistra
            new Line(1, 255 , 0), //Decima linea verticale: nell'anello medio, colonna più a sinistra
            new Line(0, 255 , 0),  //Undicesima linea verticale: nell'anello interno, colonna più a sinistra
            new Line(255, 0, 1),  //Dodicesima linea verticale: ponte superiore (riga 0, colonna 1)
            new Line(255, 2 , 1),  //Tredicesima linea verticale: ponte inferiore (riga 2, colonna 1)
            new Line(0, 255 , 2),  //Quattordicesima linea verticale: nell'anello interno, colonna più a destra
            new Line(1, 255 , 2),  //Quindicesima linea verticale: nell'anello medio, colonna più a destra
            new Line(2, 255 , 2),  //Sedicesima linea verticale: nell'anello esterno, colonna più a destra
        };

        public static readonly int[,] lines_to_intersection = {
            {8, 11, 15},//Da orizzontali a verticali
            {9, 11, 14},
            {10, 11, 13},
            {8, 9, 10},
            {13, 14, 15},
            {10, 12, 13},
            {9, 12, 14},
            {8, 12, 15},
            {0, 3, 7},//Da verticali a orizzontali
            {1, 3, 6},
            {2, 3, 5},
            {0, 1 ,2},
            {5, 6, 7},
            {2, 3, 5},
            {1, 4, 6},
            {0, 4, 7}
        };

        private static readonly List<(byte, byte, byte)>[] line_to_positions = new List<(byte, byte, byte)>[16];

        static LineUtils()
        {
            for (int l = 0; l < 16; l++)
            {
                line_to_positions[l] = new List<(byte, byte, byte)>();

                if (lines[l].ColConstraint == 255)
                {
                    line_to_positions[l].Add((lines[l].RingContraint, lines[l].RowConstraint, 0));
                    line_to_positions[l].Add((lines[l].RingContraint, lines[l].RowConstraint, 1));
                    line_to_positions[l].Add((lines[l].RingContraint, lines[l].RowConstraint, 2));
                }
                else if (lines[l].RowConstraint == 255)
                {
                    line_to_positions[l].Add((lines[l].RingContraint, 0, lines[l].ColConstraint));
                    line_to_positions[l].Add((lines[l].RingContraint, 1, lines[l].ColConstraint));
                    line_to_positions[l].Add((lines[l].RingContraint, 2, lines[l].ColConstraint));
                }
                else if (lines[l].RingContraint == 255)
                {
                    line_to_positions[l].Add((0, lines[l].RowConstraint, lines[l].ColConstraint));
                    line_to_positions[l].Add((1, lines[l].RowConstraint, lines[l].ColConstraint));
                    line_to_positions[l].Add((2, lines[l].RowConstraint, lines[l].ColConstraint));
                }
            }
        }

        public static List<(byte, byte, byte)> GetPositionsFromLine(int l)
        {
            return line_to_positions[l];
        }

        public static ISet<(byte, byte, byte)> GetPositions(IEnumerable<int> lines)
        {
            ISet<(byte, byte, byte)> res = new HashSet<(byte, byte, byte)>();
            foreach (int l in lines)
            {
                res.UnionWith(GetPositionsFromLine(l));
            }
            return res;
        }



    }
}
