﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.evaluation
{
    /// <summary>
    /// Classe che contiene la funzione di valutazione per il NineMen's Morris STANDARD.
    /// Implementazione della funzione euristica proposta da Simona-Alexandra PETCU e Stefan HOLBAN
    /// (Computer Science and Engineering, Politehnica University of Timisoara)
    /// </summary>
    public static class NineMenEvaluation
    {

        public static readonly int[] PHASE_1_WEIGHTS = { 18, 26, 1, 6, 12, 7 };
        public static readonly int[] PHASE_2_WEIGHTS = { 14, 43, 10, 8, 7, 42, 1086 };
        public static readonly int[] PHASE_3_WEIGHTS = { 10, 1, 16, 1190 };

        //TODO: aggiungere anche FreeConnectionLevel e ConnectionLevel e risistemare i pesi

        public static int EvaluateForPlayer(State state, Checker player)
        {
            //TODO: Distinguere le fasi //Phase phase = player == MacinaEnvironment.GetInstance().Checker ? state.OwnCurrentPhase : state.OpponentCurrentPhase;
            Phase phase = player == MacinaEnvironment.GetInstance().Checker ? state.CurrentPhase : state.CurrentPhase;
            switch (phase)
            {
                case Phase.FIRST:
                    return EvaluateFirstPhaseForPlayer(state, player);
                case Phase.SECOND:
                    return EvaluateSecondPhaseForPlayer(state, player);
                case Phase.FINAL:
                    return EvaluateFinalPhaseForPlayer(state, player);
            }
            return -1;
        }

        private static int EvaluateFirstPhaseForPlayer(State state, Checker player)
        {
            int res = 0;

            //R1 Closed Morris
            //if (state.Features.ClosedMorris(player))
            //    res += PHASE_1_WEIGHTS[0];

            //R2 Morrises Number
            res += PHASE_1_WEIGHTS[1] * state.Features.GetMorrisLines(player).Count;

            //R3 Number of Blocked Opponent Pieces
            res += PHASE_1_WEIGHTS[2] * state.Features.StuckOpponentPieces(player);

            //R4 Pieces number
            res += PHASE_1_WEIGHTS[3] * state.Features.PiecesNumber(player);

            //R5 Number of two-pieces configs
            res += PHASE_1_WEIGHTS[4] * state.Features.TwoPiecesConfigNumber(player);

            //R6 Number of three-pieces configs
            res += PHASE_1_WEIGHTS[5] * state.Features.ThreePiecesConfigNumber(player);

            return res;
        }

        private static int EvaluateSecondPhaseForPlayer(State state, Checker player)
        {
            int res = 0;

            //R1: Closed morris
            //if (state.Features.ClosedMorris(player))
            //    res += PHASE_2_WEIGHTS[0];

            //R2: Morris number
            res += PHASE_2_WEIGHTS[1] * state.Features.GetMorrisLines(player).Count;

            //R3: Number of blocked opponent's pieces
            res += PHASE_2_WEIGHTS[2] * state.Features.StuckOpponentPieces(player);

            //R4: pieces number
            res += PHASE_2_WEIGHTS[3] * state.Features.PiecesNumber(player);

            //R5: Opened morris
            //if(state.Features.OpenedMorris(player))
            //    res += PHASE_2_WEIGHTS[4];

            //R6: Double morris
            if (state.Features.DoubleMorris(player))
                res += PHASE_2_WEIGHTS[5];

            //R7: Winning config
            if (state.IsGoal())
                res += PHASE_2_WEIGHTS[6];

            return res;
        }

        private static int EvaluateFinalPhaseForPlayer(State state, Checker player)
        {
            int res = 0;

            //R1: 2 pieces configs
            res += PHASE_3_WEIGHTS[0] * state.Features.TwoPiecesConfigNumber(player);

            //R2: 3 pieces configs
            res += PHASE_3_WEIGHTS[1] * state.Features.ThreePiecesConfigNumber(player);

            //R3: Closed Morris
            //if(state.Features.ClosedMorris(player))
            //    res += PHASE_3_WEIGHTS[2];

            //R4 Winning config
            if (state.IsGoal())
                res += PHASE_3_WEIGHTS[3];

            return res;
        }

    }
}
