﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.evaluation
{
    public partial class NineMenFeaturesExtractor
    {
        private static class NineMenInternal
        {
            
            

        /// <summary>
        /// Dato uno stato e un giocatore, restituisce il numero di pedine bloccate del suo avversario
        /// </summary>
        /// <param name="state">Lo stato di gioco</param>
        /// <param name="player">Il giocatore considerato</param>
        /// <returns>Numero di pedine bloccate del suo avversario</returns>
        public static int StuckOpponentPieces(State state, Checker player)
        {
            Checker opponent = player.Opposite();
            int stuckPieces = 0;

                for (byte ring = 0; ring < 3; ring++)
                    for (byte row = 0; row < 3; row++)
                        for (byte col = 0; col < 3; col++)
                        {
                            if (row == 1 && col == 1)
                                continue;//Salta le posizioni invalide

                            if (opponent != state.Grid[ring, row, col])
                                continue;//Salta tutto ciò che non contiene pedine avversarie

                            if (state.Grid.FreeConnectionsOf(ring, row, col).Count == 0)
                                stuckPieces++;
                        }

                return stuckPieces;
            }

            public static int FreeConnectionLevel(State state, Checker player)
            {
                Checker opponent = player.Opposite();
                int freeConnectionLevel = 0;

                for (byte ring = 0; ring < 3; ring++)
                    for (byte row = 0; row < 3; row++)
                        for (byte col = 0; col < 3; col++)
                        {
                            if (row == 1 && col == 1)
                                continue;//Salta le posizioni invalide

                            if (opponent != state.Grid[ring, row, col])
                                continue;//Salta tutto ciò che non contiene pedine avversarie

                            freeConnectionLevel+= state.Grid.FreeConnectionsOf(ring, row, col).Count;
                        }

                return freeConnectionLevel;
            }

            public static int ConnectionLevel(State state, Checker player)
            {
                Checker opponent = player.Opposite();
                int connectionLevel = 0;

                for (byte ring = 0; ring < 3; ring++)
                    for (byte row = 0; row < 3; row++)
                        for (byte col = 0; col < 3; col++)
                        {
                            if (row == 1 && col == 1)
                                continue;//Salta le posizioni invalide

                            if (opponent != state.Grid[ring, row, col])
                                continue;//Salta tutto ciò che non contiene pedine avversarie

                            connectionLevel += state.Grid.ConnectionsOf(ring, row, col).Count;
                        }

                return connectionLevel;
            }

            /// <summary>
            /// Dato uno stato e un giocatore, restituisce il numero di pezzi del giocatore presenti sulla plancia
            /// </summary>
            /// <param name="state">Lo stato di gioco</param>
            /// <param name="player">Il giocatore considerato</param>
            /// <returns>Numero di pedine del giocatore presenti sulla plancia</returns>
            public static int PiecesNumber(State state, Checker player)
            {
                int res = 0;

                for (byte ring = 0; ring < 3; ring++)
                {
                    for (byte row = 0; row < 3; row++)
                    {
                        for (byte col = 0; col < 3; col++)
                        {
                            if (state.Grid[ring, row, col] == player)
                                res++;
                        }
                    }
                }

                return res;
            }

            /// <summary>
            /// Dato uno stato e un giocatore, viene restituito vero se il giocatore ha appena aperto un morris (se la pedina mossa faceva parte di un morris)
            /// </summary>
            /// <param name="state">Lo stato di gioco</param>
            /// <param name="player">Il giocatore considerato</param>
            /// <returns>True se il giocatore ha appena aperto un morris</returns>
            //public static bool OpenedMorris(State state, Checker player)
            //{
            //    var lastMovePlayer = state.LastMoveDone.Target;
            //    if (player != lastMovePlayer)
            //        return false; //Se l'ultimo giocatore che ha mosso non è il giocatore che si sta considerando è impossibile che questi abbia aperto un morris in questo turno.


            //    if (state.LastMoveDone.StepsCount < 2)
            //        return false;

            //    var lastFromMove = state.LastMoveDone[1];
            //    if (lastFromMove.Type != StepTypes.FROM)
            //        return false;

            //    byte fromRing = lastFromMove.Ring;
            //    byte fromRow = lastFromMove.Row;
            //    byte fromCol = lastFromMove.Col;

            //    //Controllo che ci fosse un morris sulla riga del from
            //    bool morris = false;
            //    if (fromRow != 1)//Se non sono sulla riga centrale
            //    {
            //        morris = true;
            //        for (byte j = 0; j < 3; j++)
            //        {
            //            if (j == fromCol)
            //                continue; //Ignoro la casella in cui ero poiché ora è sicuramente vuota
            //            morris = morris && player == state.Grid[fromRing, fromRow, j];
            //        }
            //    }
            //    if (morris) return true;

            //    //Controllo che ci fosse in morris sulla colonna del from

            //    if (fromCol != 1)//Se non sono sulla colonna centrale
            //    {
            //        morris = true;
            //        for (byte i = 0; i < 3; i++)
            //        {
            //            if (i == fromRow)
            //                continue; //Ignoro la casella in cui ero poiché ora è sicuramente vuota
            //            morris = morris && player == state.Grid[fromRing, i, fromCol];
            //        }
            //    }
            //    if (morris) return true;

            //    //Controllo che ci fosse un morris su un ponte
            //    if (1 == fromCol || 1 == fromRow)//Se ero su un ponte
            //    {
            //        morris = true;
            //        for (byte r = 0; r < 3; r++)
            //        {
            //            if (r == fromRing)
            //                continue; //Ignoro la casella in cui ero poiché ora è sicuramente vuota
            //            morris = morris && player == state.Grid[r, fromRow, fromCol];
            //        }
            //    }
            //    return morris;
            //}

            /// <summary>
            /// Dato uno stato e un giocatore, restituisce il numero di two pieces config (numero di linee lungo cui il giocatore può fare morris piazzando subito una pedina)
            /// </summary>
            /// <param name="state">Lo stato di gioco</param>
            /// <param name="player">Il giocatore considerato</param>
            /// <returns>Numero di two pieces config</returns>
            public static int TwoPiecesConfigNumber(State state, Checker player)
            {

                int res = 0;
                int tempCount = 0;
                Checker tempCheck;

                Checker opponent = player.Opposite();

                //Controllo i two pieces sulle righe
                for (byte ring = 0; ring < 3; ring++)
                {
                    for (byte row = 0; row < 3; row++)
                    {
                        if (row != 1)//Se non sono sulla riga centrale
                        {
                            tempCount = 0;
                            for (byte j = 0; j < 3; j++)
                            {
                                tempCheck = state.Grid[ring, row, j];
                                if (player == tempCheck)
                                    tempCount++;
                                else if (opponent == tempCheck)
                                {
                                    tempCount = 0;
                                    break;
                                }
                            }
                            if (tempCount == 2) res++;
                        }
                    }
                }

                //Controllo i two pieces sulle colonne
                for (byte ring = 0; ring < 3; ring++)
                {
                    for (byte col = 0; col < 3; col++)
                    {
                        if (col != 1)//Se non sono sulla colonna centrale
                        {
                            tempCount = 0;
                            for (byte i = 0; i < 3; i++)
                            {
                                tempCheck = state.Grid[ring, i, col];
                                if (player == tempCheck)
                                    tempCount++;
                                else if (opponent == tempCheck)
                                {
                                    tempCount = 0;
                                    break;
                                }
                            }
                            if (tempCount == 2) res++;
                        }
                    }
                }

                //Controllo i morris sui ponti
                for (byte row = 0; row < 3; row++)
                {
                    for (byte col = 0; col < 3; col++)
                    {
                        if (row == 1 || col == 1 && !(row == 1 && col == 1))
                        {
                            tempCount = 0;
                            for (byte r = 0; r < 3; r++)
                            {
                                tempCheck = state.Grid[r, row, col];
                                if (player == tempCheck)
                                    tempCount++;
                                else if (opponent == tempCheck)
                                {
                                    tempCount = 0;
                                    break;
                                }
                            }
                            if (tempCount == 2) res++;
                        }
                    }
                }

                return res;
            }

            

            private static int CountOnLine(State s, Checker player, int line)
            {
                Checker opponent = player.Opposite();

            if (LineUtils.lines[line].ColConstraint == 255)
                return CountHorizontal(s, player, opponent, LineUtils.lines[line].RingContraint, LineUtils.lines[line].RowConstraint);
            else if(LineUtils.lines[line].RowConstraint == 255)
                    return CountVertical(s, player, opponent, LineUtils.lines[line].RingContraint, LineUtils.lines[line].ColConstraint);
            else if(LineUtils.lines[line].RingContraint == 255)
                    return CountBridge(s, player, opponent, LineUtils.lines[line].RowConstraint, LineUtils.lines[line].ColConstraint);

                return -1;
            }

            private static int CountVertical(State s, Checker player, Checker opponent, byte ring, byte col)
            {
                int count = 0;
                for (byte row = 0; row < 3; row++)
                {
                    if (opponent == s.Grid[ring, row, col])
                        return -1;

                    if (player == s.Grid[ring, row, col])
                        count++;
                }
                return count;
            }

            private static int CountHorizontal(State s, Checker player, Checker opponent, byte ring, byte row)
            {
                int count = 0;
                for (byte col = 0; col < 3; col++)
                {
                    if (opponent == s.Grid[ring, row, col])
                        return -1;

                    if (player == s.Grid[ring, row, col])
                        count++;
                }
                return count;
            }

            private static int CountBridge(State s, Checker player, Checker opponent, byte row, byte col)
            {
                int count = 0;
                for (byte ring = 0; ring < 3; ring++)
                {
                    if (opponent == s.Grid[ring, row, col])
                        return -1;

                    if (player == s.Grid[ring, row, col])
                        count++;
                }
                return count;
            }

            /// <summary>
            /// Restituisce true se sulla linea considerata vi sono due pedine del giocatore e rimane un posto vuoto.
            /// La linea passante per la caselal vuota perpendicolare alla linea data viene restituita in output in line_to_check.
            /// La linea passante per la casella vuota perpendicolare alla linea data viene restituita in output in line_to_check.
            /// </summary>
            /// <param name="state"></param>
            /// <param name="player"></param>
            /// <param name="opponent"></param>
            /// <param name="line"></param>
            /// <param name="line_to_check"></param>
            /// <returns></returns>
            private static bool ThreePiecesConfigNumberHelper(State state, Checker player, int line, out int[] lines_to_check)
            {
                byte ringMin = 0, ringMax = 2;
                byte rowMin = 0, rowMax = 2;
                byte colMin = 0, colMax = 2;

            if(CountOnLine(state, player, line) == 2)
            {
                if(LineUtils.lines[line].RingContraint != 255)
                {
                    ringMin = LineUtils.lines[line].RingContraint; ringMax = LineUtils.lines[line].RingContraint;
                }
                else if(LineUtils.lines[line].RowConstraint != 255)
                {
                    rowMin = LineUtils.lines[line].RowConstraint; rowMax = LineUtils.lines[line].RowConstraint;
                }
                else if(LineUtils.lines[line].ColConstraint != 255)
                {
                    colMin = LineUtils.lines[line].ColConstraint; colMax = LineUtils.lines[line].ColConstraint;
                }
 
                for(byte ring=ringMin; ring <= ringMax; ring++)
                {
                    for(byte row=rowMin; row <= rowMax; row++)
                    {
                        for(byte col = colMin; col <= colMax; col++)
                        {
                            if(player != state.Grid[ring, row, col])
                            {
                                lines_to_check = new int[3];
                                for(int j = 0; j < 3; j++)
                                    lines_to_check[j] = LineUtils.lines_to_intersection[line, j];
                                
                                return true;
                            }
                        }
                    }
                }
            }

                lines_to_check = null;
                return false;
            }

            /// <summary>
            /// Dato uno stato e un giocatore, restituisce il numero di three pieces config (configurazione doppie di two pieces config = è subito possibile fare morris in due linee a scelta)
            /// </summary>
            /// <param name="state">Lo stato di gioco</param>
            /// <param name="player">Il giocatore considerato</param>
            /// <returns>Il numero di three pieces config</returns>
            public static int ThreePiecesConfigNumber(State state, Checker player)
            {
                int res = 0;


                int[] l_inner = null;
                for (int l = 0; l < 8; l++)//Ciclo sulle linee orizzontali
                {
                    if (ThreePiecesConfigNumberHelper(state, player, l, out l_inner))
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (ThreePiecesConfigNumberHelper(state, player, l_inner[j], out int[] temp))//(L'inner in out non serve)
                            {
                                res++;
                            }
                        }
                    }
                }

                return res;
            }

        /// <summary>
        /// Dato uno stato e un giocatore, restituisce il numero di doppi morris del giocatore presenti sulla plancia
        /// </summary>
        /// <param name="state">Lo stato di gioco</param>
        /// <param name="player">Il giocatore considerato</param>
        /// <returns>Il numero di doppi morris</returns>
        public static bool DoubleMorris(State state, Checker player, ISet<int> morrisLines)
        {
            foreach(int ml in morrisLines)//Ciclo sulle linee con dei morris
            {
                for(int l_inner = 0; l_inner <3; l_inner++)
                {
                    if (3 == CountOnLine(state, player, LineUtils.lines_to_intersection[ml, l_inner]) )
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Metodo per restituire le linee contenenti un morris
        /// </summary>
        /// <param name="state">Lo stato di gioco</param>
        /// <param name="player">Il giocatore considerato</param>
        /// <returns>Una collezione di interi, ciascun intero corrisponde alla linea secondo la convenzione stabilita</returns>
        public static SortedSet<int> GetMorrisLinesIndex(State state, Checker player)
        {
            //TODO: x Kevin
            SortedSet<int> res = new SortedSet<int>();
            for (int l = 0; l < 16; l++)//Ciclo sulle linee
            {
                if (3 == CountOnLine(state, player, l))//C'è un morris
                {
                    res.Add(l); //Aggiungo la linea l a res
                }
            }

                return res;
            }

        }
    }
    
}
