﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.evaluation
{
    partial class NineMenFeaturesExtractor : IFeaturesExtractor
    {

        private readonly Dictionary<Checker, ISet<int>> _linesNumbers = new Dictionary<Checker, ISet<int>>();
        private readonly State _refState;

        public NineMenFeaturesExtractor(State refState)
        {
            _refState = refState;
        }

        public ISet<int> GetMorrisLines(Checker player)
        {
            if (!_linesNumbers.ContainsKey(player))
                _linesNumbers.Add(player, NineMenInternal.GetMorrisLinesIndex(_refState, player));
            return _linesNumbers[player];
        }

        

        public bool OpenedMorris(Checker player)
        {
            return false; //NineMenInternal.OpenedMorris(_refState, player);
        }

        public int FreeConnectionLevel(Checker player)
        {
            return NineMenInternal.FreeConnectionLevel(_refState, player);
        }

        public int ConnectionLevel(Checker player)
        {
            return NineMenInternal.ConnectionLevel(_refState, player);
        }

        public int PiecesNumber(Checker player)
        {
            return NineMenInternal.PiecesNumber(_refState, player);
        }

        public int StuckOpponentPieces(Checker player)
        {
            return NineMenInternal.StuckOpponentPieces(_refState, player);
        }

        public int ThreePiecesConfigNumber(Checker player)
        {
            return NineMenInternal.ThreePiecesConfigNumber(_refState, player);
        }

        public int TwoPiecesConfigNumber(Checker player)
        {
            return NineMenInternal.TwoPiecesConfigNumber(_refState, player);
        }

        public bool DoubleMorris(Checker player)
        {
            return NineMenInternal.DoubleMorris(_refState, player, GetMorrisLines(player));
        }
    }
}
