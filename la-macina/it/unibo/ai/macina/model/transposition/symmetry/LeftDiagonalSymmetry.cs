﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition.symmetry
{
	public class LeftDiagonalSymmetry : Symmetry
	{

		#region Constructrs

		public LeftDiagonalSymmetry() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			return (ring, col, row);
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			return (ring, col, row);
		}

		public override string ToString()
		{
			return "LeftDiagonalSymmetry";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.LEFT_DIAGONAL_SYMMETRY;
		}

		#endregion

	}
}
