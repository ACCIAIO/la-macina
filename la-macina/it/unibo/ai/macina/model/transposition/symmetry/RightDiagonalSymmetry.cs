﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition.symmetry
{
	public class RightDiagonalSymmetry : Symmetry
	{
		#region Constructors

		public RightDiagonalSymmetry() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			return (ring, (byte)(2 - col), (byte)(2 - row));
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			return (ring, (byte)(2 - col), (byte)(2 - row));
		}

		public override string ToString()
		{
			return "RightDiagonalSymmetry";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY;
		}

		#endregion

	}
}
