﻿using la_macina.it.unibo.ai.macina.model.grid;
using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition.symmetry
{
	public class CentralSymmetry : Symmetry
	{
		
		#region Costructors

		public CentralSymmetry() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			IGrid grid = MacinaEnvironment.GetInstance().GameMode.EmptyGrid;
			byte maxRing = (byte)(grid.RingCount - 1);
			return ((byte)(maxRing - ring), row, col);
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
            IGrid grid = MacinaEnvironment.GetInstance().GameMode.EmptyGrid;
            byte maxRing = (byte)(grid.RingCount - 1);
			return ((byte)(maxRing - ring), row, col);
		}

		public override string ToString()
		{
			return "CentralSymmetry";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.CENTRAL_SYMMETRY;
		}

		#endregion

	}
}
