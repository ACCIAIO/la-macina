﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition.symmetry
{
	public class VerticalSymmetry : Symmetry
	{

		#region Constructors

		public VerticalSymmetry() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			return (ring, row, (byte)(2 - col));
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			return (ring, row, (byte)(2 - col));
		}

		public override string ToString()
		{
			return "VerticalSymmetry";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.VERTICAL_SYMMETRY;
		}

		#endregion

	}
}
