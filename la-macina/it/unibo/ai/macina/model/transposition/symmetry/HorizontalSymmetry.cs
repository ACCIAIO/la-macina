﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition.symmetry
{
	public class HorizontalSymmetry : Symmetry
	{

		#region Costructors

		public HorizontalSymmetry() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			return (ring, (byte)(2 - row), col);
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			return (ring, (byte)(2 - row), col);
		}

		public override string ToString()
		{
			return "HorizontalSymmetry";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.HORIZONTAL_SYMMETRY;
		}
		#endregion



	}
}
