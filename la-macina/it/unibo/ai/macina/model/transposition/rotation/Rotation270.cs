﻿using System;
using System.Collections.Generic;
using System.Text;
using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.transposition.symmetry;

namespace la_macina.it.unibo.ai.macina.model.transposition.rotation
{
	public class Rotation270 : Rotation
	{

		#region Costructors

		public Rotation270() : base() { }

		#endregion

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			// 2 è dato da Lenght(riga/colonna) - 1 = 3 - 1 = 2
			return (ring, (byte)(2 - col), row);
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			// 2 è dato da Lenght(riga/colonna) - 1 = 3 - 1 = 2
			return (ring, col, (byte)(2 - row));
		}

		public override string ToString()
		{
			return "Rotation270";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.ROTATION_270;
		}

		#endregion



	}
}
