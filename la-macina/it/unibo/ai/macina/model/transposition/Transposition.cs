﻿using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.transposition.rotation;
using la_macina.it.unibo.ai.macina.model.transposition.symmetry;
using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition
{
	/// <summary>
	/// Classe base per tutte le trasposizioni
	/// </summary>
	public abstract class Transposition
	{
		protected Transposition() { }

		#region Members

		protected static readonly Dictionary<Transposition, Transposition> equivTransp = new Dictionary<Transposition, Transposition>();

		#endregion

		#region Public methods

		/// <summary>
		/// Data una posizione nella griglia, trova quella corrispondente applicando la trasposizione
		/// </summary>
		public abstract (byte, byte, byte) Apply(byte ring, byte row, byte col);

		/// <summary>
		/// Data una posizione trasposta della griglia, trova quella originale applicando
		/// la trasposizione al contrario
		/// </summary>
		public abstract (byte, byte, byte) Revert(byte ring, byte row, byte col);

		/// <summary>
		/// Restituisce l'enumerativo associato alla trasposizione
		/// </summary>
		public abstract TranspositionEnum GetEnum();

		#endregion

		#region Private methods

		/// <summary>
		/// Restituisce la trasposizione risultato dalla somma dell'attuale
		/// trasposizione (this) con quella ricevuta, accedendo al dizionario delle equivalenze.
		/// </summary>
		/// <param name="transposition"></param>
		/// <returns></returns>
		private Transposition Sum(Transposition transposition)
		{
			return TranspositionEquivalenceTable.GetEquivalentTransposition(this, transposition);
		}

		#endregion


		#region Operators overload

		public static Transposition operator +(Transposition thisTransposition, Transposition otherTransposition)
		{
			return thisTransposition.Sum(otherTransposition);
		}

		#endregion

	}

	/// <summary>
	/// Classe factory per le trasposizioni
	/// </summary>
	public static class TranspositionFactory
	{

		#region Static region

		private static readonly Dictionary<TranspositionEnum, Transposition> types = new Dictionary<TranspositionEnum, Transposition>();

		static TranspositionFactory()
		{
			types[TranspositionEnum.IDENTITY] = new Identity();
			types[TranspositionEnum.VERTICAL_SYMMETRY] = new VerticalSymmetry();
			types.Add(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY, new RightDiagonalSymmetry());
			types.Add(TranspositionEnum.HORIZONTAL_SYMMETRY, new HorizontalSymmetry());
			types.Add(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY, new LeftDiagonalSymmetry());
			types.Add(TranspositionEnum.CENTRAL_SYMMETRY, new CentralSymmetry());
			types.Add(TranspositionEnum.ROTATION_90, new Rotation90());
			types.Add(TranspositionEnum.ROTATION_180, new Rotation180());
			types.Add(TranspositionEnum.ROTATION_270, new Rotation270());
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Restituisce la trasposizione associata all'enumerativo
		/// </summary>
		public static Transposition GetInstance(TranspositionEnum transpositionEnum)
		{
			return types[transpositionEnum];
		}

		public static ICollection<Transposition> GetAll()
		{
			return types.Values;
		}

		#endregion

	}

	public enum TranspositionEnum : byte
	{
		IDENTITY = 0,
		VERTICAL_SYMMETRY,
		RIGHT_DIAGONAL_SYMMETRY,
		HORIZONTAL_SYMMETRY,
		LEFT_DIAGONAL_SYMMETRY,
		CENTRAL_SYMMETRY,
		ROTATION_90,
		ROTATION_180,
		ROTATION_270
	};
}
