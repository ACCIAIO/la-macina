﻿using la_macina.it.unibo.ai.macina.model.transposition.symmetry;
using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition
{
	public class TranspositionStack
	{
		#region Member

		// la trasposizione in essere
		private Transposition _transposition;

		// variabile di appoggio, evito di chiederla alla TranspositionFactory ogni volta
		private Transposition _centralSymmetry;

		// indica se c'è o meno una simmetria centrale
		private bool _isThereCentralSymmetry;
		
		#endregion

		#region Constructors

		public TranspositionStack()
		{
            Init();
		}

		#endregion

		#region Properties

		public Transposition GetTransposition => _transposition;

		public bool IsThereCentralSymmetry => _isThereCentralSymmetry;

        #endregion

        #region Internal methods

        /// <summary>
        /// Cambia lo stato della simmetria centrale
        /// </summary>
        private void ChangeCentralSymmetry()
        {
            _isThereCentralSymmetry = !_isThereCentralSymmetry;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Applica la trasposizione alle coordinate
        /// </summary>
        /// <returns>Restituisce le nuove coordinate</returns>
        public (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			if (_isThereCentralSymmetry)
				(ring, row, col) = _centralSymmetry.Apply(ring, row, col);
			return _transposition.Apply(ring, row, col);
		}

		/// <summary>
		/// Applica la trasposizione alle coordinate
		/// </summary>
		/// <returns>Restituisce le nuove coordinate</returns>
		public Move Apply(Move move)
		{
			Move m = Move.Of(move.Target, move.Phase);
			(byte, byte, byte) to = Apply(move[0].Ring, move[0].Row, move[0].Col);
			m = m.To(to.Item1, to.Item2, to.Item3);
			if (move.Phase == Phase.FIRST)
			{
				if (move.StepsCount == 2)
				{
					(byte, byte, byte) andRemove = Apply(move[1].Ring, move[1].Row, move[1].Col);
					m = m.AndRemove(andRemove.Item1, andRemove.Item2, andRemove.Item3);
				}
			}
			else
			{
				(byte, byte, byte) from = Apply(move[1].Ring, move[1].Row, move[1].Col);
				m = m.From(from.Item1, from.Item2, from.Item3);
				if(move.StepsCount == 3)
				{
					(byte, byte, byte) andRemove = Apply(move[2].Ring, move[2].Row, move[2].Col);
					m = m.AndRemove(andRemove.Item1, andRemove.Item2, andRemove.Item3);
				}
			}

			return m;
		}

		/// <summary>
		/// Applica la trasposizione inversa alle coordinate
		/// </summary>
		/// <returns>Restituisce le coordinate originarie</returns>
		public (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			if (_isThereCentralSymmetry)
				(ring, row, col) = _centralSymmetry.Revert(ring, row, col);
			return _transposition.Revert(ring, row, col);
		}

		/// <summary>
		/// Aggiunge la nuova trasposizione allo stack
		/// In realtà sostituisce la trasposizione attuale con la trasposizione
		/// equivalente alla somma di quella attuale e di quella nuova.
		/// Se la nuova è una 'CentralSymmetry', la trasposizione attuale non varia,
		/// varia solamente lo stato della simmetria centrale (true/false)
		/// </summary>
		/// <param name="transposition">La trasposizione da aggiungere</param>
		public void Add(Transposition transposition) {
			if (transposition is CentralSymmetry)
				ChangeCentralSymmetry();
			else
				_transposition = _transposition + transposition;
		}

        public void Init()
        {
            _transposition = TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY);
            _centralSymmetry = TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY);
            _isThereCentralSymmetry = false;
        }

        #endregion
    }
}
