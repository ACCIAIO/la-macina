﻿using la_macina.it.unibo.ai.macina.model.transposition.symmetry;
using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition
{
	public class Identity : Transposition
	{

		#region Constructors

		public Identity() : base()
		{
			
		}

		#endregion

		public static Dictionary<Transposition,Transposition> GetEquiv => equivTransp;

		#region Public methods

		public override (byte, byte, byte) Apply(byte ring, byte row, byte col)
		{
			return (ring, row, col);
		}

		public override (byte, byte, byte) Revert(byte ring, byte row, byte col)
		{
			return (ring, row, col);
		}	

		public override string ToString()
		{
			return "Identity";
		}

		public override TranspositionEnum GetEnum()
		{
			return TranspositionEnum.IDENTITY;
		}

		#endregion


	}
}
