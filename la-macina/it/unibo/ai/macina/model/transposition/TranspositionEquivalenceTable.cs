﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.transposition
{
    public static class TranspositionEquivalenceTable
    {
		#region Static region
		
		/// <summary>
		/// Rappresenta un dizionario di dizionari(Type,Transposition) accessibile tramite TranspositionEnum.
		/// Scelto un emumerativo, si ottiene un dizionario con coppie (TipoTrasposizione, Trasposizione) che
		/// indicano tutte le equivalenze possibili, nella forma 'Enum + Tipo = Trasposizione'.
		/// Il Type è stato messo per scatenare il polimorfismo, usando invece 'Trasposition' come key non
		/// si otteneva l'effetto desiderato.
		/// </summary>
		private static readonly Dictionary<TranspositionEnum, Dictionary<Type, Transposition>> equivalences = new Dictionary<TranspositionEnum, Dictionary<Type, Transposition>>();

		/// <summary>
		/// Creazione del dizionario.
		/// </summary>
		static TranspositionEquivalenceTable() {
			Dictionary<Type, Transposition> emptyEquiv = new Dictionary<Type, Transposition>();
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			//emptyEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY));

			equivalences.Add(TranspositionEnum.IDENTITY, emptyEquiv);

			Dictionary<Type, Transposition> rotation90Equiv = new Dictionary<Type, Transposition>();
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			//rotation90Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));

			equivalences.Add(TranspositionEnum.ROTATION_90, rotation90Equiv);

			Dictionary<Type, Transposition> rotation180Equiv = new Dictionary<Type, Transposition>();
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			//rotation180Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));

			equivalences.Add(TranspositionEnum.ROTATION_180, rotation180Equiv);

			Dictionary<Type, Transposition> rotation270Equiv = new Dictionary<Type, Transposition>();
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			//rotation270Equiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));

			equivalences.Add(TranspositionEnum.ROTATION_270, rotation270Equiv);

			Dictionary<Type, Transposition> verticalEquiv = new Dictionary<Type, Transposition>();
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			//verticalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));

			equivalences.Add(TranspositionEnum.VERTICAL_SYMMETRY, verticalEquiv);

			Dictionary<Type, Transposition> rightDiagonalEquiv = new Dictionary<Type, Transposition>();
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			//rightDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));

			equivalences.Add(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY, rightDiagonalEquiv);

			Dictionary<Type, Transposition> horizontalEquiv = new Dictionary<Type, Transposition>();
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			//horizontalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));

			equivalences.Add(TranspositionEnum.HORIZONTAL_SYMMETRY, horizontalEquiv);

			Dictionary<Type, Transposition> leftDiagonalEquiv = new Dictionary<Type, Transposition>();
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));
			//leftDiagonalEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));

			equivalences.Add(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY, leftDiagonalEquiv);

			//Dictionary<Type, Transposition> centralEquiv = new Dictionary<Type, Transposition>();
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_90));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_180));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.ROTATION_270));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.VERTICAL_SYMMETRY));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.RIGHT_DIAGONAL_SYMMETRY));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.HORIZONTAL_SYMMETRY));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.LEFT_DIAGONAL_SYMMETRY));
			//centralEquiv.Add(TranspositionFactory.GetInstance(TranspositionEnum.CENTRAL_SYMMETRY).GetType(), TranspositionFactory.GetInstance(TranspositionEnum.IDENTITY));

			//equivs.Add(TranspositionEnum.CENTRAL_SYMMETRY, centralEquiv);


		}
		
		/// <summary>
		/// Restituisce la trasposizione equivalente a
		/// "<paramref name="firstTransposition"/> + <paramref name="secondTransposition"/>".
		/// Se la coppia non è presente viene lanciata un'eccezione
		/// </summary>
		/// <param name="firstTransposition"></param>
		/// <param name="secondTransposition"></param>
		/// <returns></returns>
		public static Transposition GetEquivalentTransposition(Transposition firstTransposition, Transposition secondTransposition) {
			return equivalences[firstTransposition.GetEnum()][secondTransposition.GetType()];
		}

		#endregion
	}
}
