﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.hashing
{
    public enum HashingFunction : byte
    {
        ZOBRIST,
        POWER
    }

    public interface IHashing<T>
    {
        long Hash(T obj);
        long UpdateHash(long oldHash, Phase oldPhase, byte oldOwnCheckers, byte oldOpponentCheckers, Move move);
	}

    public class HashFunctionsFactory
    {
        private readonly Dictionary<HashingFunction, IHashing<State>> _hashes = new Dictionary<HashingFunction, IHashing<State>>();
        private readonly IHashing<State> _zobrist;
        private readonly IHashing<State> _power;

        public HashFunctionsFactory(byte men)
        {
            _hashes.Add(HashingFunction.ZOBRIST, new ZobristHashing(men));
            _hashes.Add(HashingFunction.POWER, new PowerHash());
        }

        public IHashing<State> GetHashingFunction(HashingFunction hf)
        {
            return _hashes[hf];
        }
    }
}
