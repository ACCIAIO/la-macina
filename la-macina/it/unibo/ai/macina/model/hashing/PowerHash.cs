﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.hashing
{
    class PowerHash : IHashing<State>
    {
        public long Hash(State obj)
        {
            var myChecker = MacinaEnvironment.GetInstance().Checker;
            long white = 0, black = 0;

            int counter = 0;
            foreach((byte, byte, byte) pos in obj.Grid.AllPositions())
            {
                var ch = obj.Grid[pos.Item1, pos.Item2, pos.Item3];
                    if (Checker.WHITE == ch)
                        white |= (long)(1 << counter);
                    else if(Checker.BLACK == ch)
                        black |= (long)(1 << counter);
                counter++;
            }
            white = white << 5;
            black = black << 5;

            white |= Checker.WHITE == myChecker ? obj.OwnCheckers : obj.OpponentCheckers;
            black |= Checker.BLACK == myChecker ? obj.OwnCheckers : obj.OpponentCheckers;

            var result = (white << 32) | black;
            result = result << 2;

            return result | (byte)obj.CurrentPhase;
        }

        public long UpdateHash(long oldHash, Phase oldPhase, byte oldOwnCheckers, byte oldOpponentCheckers, Move move)
        {
            throw new NotImplementedException();
        }
    }
}
