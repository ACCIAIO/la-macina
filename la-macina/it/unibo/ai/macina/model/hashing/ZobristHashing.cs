﻿using System;
using System.Collections.Generic;
using System.Text;

namespace la_macina.it.unibo.ai.macina.model.hashing
{
    /// <summary>
    /// Implementazione dello Zobrist Hashing specifica per il nine men morris. I valori importanti sono la posizione e il tipo delle pedine e la fase
    /// </summary>
    public class ZobristHashing : IHashing<State>
    {

        private readonly long[,,,] board_table = new long[3, 3, 3, 2];//Ring, row, col, checker_type
        private readonly long[] phase_table = new long[3];

        private readonly long[] myCheckers_table;//Contributi a seconda del numero di pedine in mano a me
        private readonly long[] opponentCheckers_table;//Contributi a seconda del numero di pedine in mano all'avversario

        private readonly long[] myEatenCheckers_table;//Contributi a seconda del numero di pedine in mano a me
        private readonly long[] opponentEatenCheckers_table;//Contributi a seconda del numero di pedine in mano all'avversario

        public ZobristHashing(byte men)
        {
            Random r = new Random(DateTime.Now.Millisecond);

            myCheckers_table = new long[men + 1];//Contribut
            opponentCheckers_table = new long[men + 1];//Con

            myEatenCheckers_table = new long[men + 1];//Cont
            opponentEatenCheckers_table = new long[men + 1];

            //Inizializzo la tabella relativa alla board
            for (int ring = 0; ring < 3; ring++)
            {
                for(int row = 0; row < 3; row++)
                {
                    for(int col = 0; col < 3; col++)
                    {
                        if( row == 1 && col == 1)
                        {
                            board_table[ring, row, col, 0] = 0;
                            board_table[ring, row, col, 1] = 0;
                        }
                        else
                        {
                            board_table[ring, row, col, 0] = (((long) r.Next()) << 32 ) + r.Next();
                            board_table[ring, row, col, 1] = (((long)r.Next()) << 32) + r.Next();
                        }
                    }
                }
            }

            //Inizializzo la tabella relativa alle fasi
            for(int p = 0; p < 3; p++)
            {
                phase_table[p] = (((long)r.Next()) << 32) + r.Next();
            }

            for(int n = 0; n <= 9; n++)
            {
                myCheckers_table[n] = (((long)r.Next()) << 32) + r.Next();
                opponentCheckers_table[n] = (((long)r.Next()) << 32) + r.Next();
                myEatenCheckers_table[n] = (((long)r.Next()) << 32) + r.Next();
                opponentEatenCheckers_table[n] = (((long)r.Next()) << 32) + r.Next();
            }

        }

        /// <summary>
        /// Restituisce l'hash di uno stato
        /// </summary>
        /// <param name="state">Lo stato</param>
        /// <returns>L'hash</returns>
        public long Hash(State state)
        {
            long hash = 0;

            Checker tempChecker;

            //Contributi dei pezzi sulla board
            for (byte ring = 0; ring < 3; ring++)
            {
                for (byte row = 0; row < 3; row++)
                {
                    for (byte col = 0; col < 3; col++)
                    {
                        tempChecker = state.Grid[ring, row, col];
                        if(Checker.INVALID != tempChecker && Checker.NONE != tempChecker)
                            hash ^= board_table[ring, row, col, (int)tempChecker - 1];
                    }
                }
            }

            //Contributo della fase:

            hash ^= phase_table[(int)state.CurrentPhase];

            hash ^= myCheckers_table[state.OwnCheckers];
            hash ^= opponentCheckers_table[state.OpponentCheckers];
            hash ^= myCheckers_table[state.OwnEatenCheckers];
            hash ^= opponentCheckers_table[state.OpponentEatenCheckers];

            return hash;
        }

        /// <summary>
        /// Funzione che dato il codice di uno stato e la fase di quello stato restituisce l'hash dello stato che si ottiene applicando la mossa specificata
        /// </summary>
        /// <param name="oldHash">L'hash dello stato</param>
        /// <param name="oldPhase">La fase dello stato</param>
        /// <param name="move">La mossa che verrà applicata</param>
        /// <returns>L'hash dello stato ottenuto applicando la mossa specificata allo stato in ingresso</returns>
        public long UpdateHash(long oldHash, Phase oldPhase, byte oldOwnCheckers, byte oldOpponentCheckers, Move move)
        {
            Checker myPlayer = MacinaEnvironment.GetInstance().Checker;

            foreach (MoveStep ms in move)
            {
                switch (ms.Type)
                {
                    case StepTypes.FROM:
                        oldHash ^= board_table[ms.Ring, ms.Row, ms.Col, move.Target == Checker.WHITE ? 0 : 1];//Viene tolta una pedina MIA
                        break;
                    case StepTypes.TO:
                        oldHash ^= board_table[ms.Ring, ms.Row, ms.Col, move.Target == Checker.WHITE ? 0 : 1];//Viene aggiunta una pedina MIA
                        break;
                    case StepTypes.ANDREMOVE:
                        oldHash ^= board_table[ms.Ring, ms.Row, ms.Col, move.Target == Checker.BLACK ? 0 : 1];//Viene tolta una pedina dell'AVVERSARIO
                        break;
                }
            }

            oldHash ^= phase_table[(int)oldPhase];
            oldHash ^= phase_table[(int)move.Phase];

            if (move.Phase == Phase.FIRST)//E' diminuito di 1 il numero di pedine
            {
                if (move.Target == myPlayer)
                {
                    oldHash ^= myCheckers_table[oldOwnCheckers];
                    oldHash ^= myCheckers_table[oldOwnCheckers - 1];
                }
                else if(move.Target == myPlayer.Opposite())
                {
                    oldHash ^= opponentCheckers_table[oldOpponentCheckers];
                    oldHash ^= opponentCheckers_table[oldOpponentCheckers - 1];
                }
                
            }

            return oldHash;
        }
    }
}
