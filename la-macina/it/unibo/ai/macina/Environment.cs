﻿using System;
using System.Collections.Generic;
using System.Linq;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.hashing;
using la_macina.it.unibo.ai.macina.model.strategy;
using la_macina.it.unibo.ai.macina.model.transposition;

namespace la_macina.it.unibo.ai.macina
{
	/// <summary>
	/// Descrive i possibili colori delle pedine
	/// </summary>
	public enum Checker : byte
	{
		NONE,
		WHITE,
		BLACK,
		INVALID
	}

	/// <summary>
	/// Classe accessoria per la gestione dello stato delle intersezioni
	/// </summary>
	public static class CheckerUtils // and extensions ;D
	{
        public static Checker Opposite(this Checker checker)
        {
            if (Checker.BLACK == checker)
                return Checker.WHITE;
            if (Checker.WHITE == checker)
                return Checker.BLACK;
            return Checker.INVALID;
        }

        public static Checker GetFromInitial(char init)
        {
            switch (init)
            {
                case 'W':
                    return Checker.WHITE;
                case 'B':
                    return Checker.BLACK;
                case 'O':
                    return Checker.NONE;
                default:
                    return Checker.INVALID;
            }
        }

		public static string ToString(this Checker checker)
		{
			switch (checker)
			{
				case Checker.NONE:
					return "( )";
				case Checker.WHITE:
					return "(W)";
				case Checker.BLACK:
					return "(B)";
				default:
					return "   ";
			}
		}
	}

	/// <summary>
	/// Mantiene le informazioni di uso globale per la partita corrente
	/// </summary>
	public class MacinaEnvironment
	{
        #region Static Region

        private readonly static Dictionary<byte, GameMode> _validGameModes = new Dictionary<byte, GameMode>
        {
            { 3, new ThreeMenMorris()},
            { 6, new SixMenMorris()},
            { 9, new NineMenMorris()},
            { 12, new TwelveMenMorris()},
        };

		private static MacinaEnvironment _env;

		public static MacinaEnvironment GetInstance()
		{
			if (null != _env)
				return _env;
			throw new InvalidOperationException("Can't access environment prior to its creation");
		}

		public static MacinaEnvironment CreateInstance(Checker checker, byte men)
		{
			if (null != _env) return _env;
			if (!_validGameModes.Any(kvp => kvp.Key == men))
				throw new ArgumentException("invalid game mode " + men + " men's morris");
			return _env = new MacinaEnvironment(_validGameModes[men], checker);
		}



		#endregion

		#region Properties 

        /// <summary>
        /// Contiene le informazioni riguardanti la modalità di gioco, oltre ad un metodo per verificare la validità
        /// di una mossa seguendo le regole della modalità stessa.
        /// </summary>
        public GameMode GameMode { get; }

		/// <summary>
		/// Numero di pedine per ogni giocatore
		/// </summary>
		public byte Men
        {
            get
            {
                return GameMode.Men;
            }
        }

		/// <summary>
		/// Colore della pedina per la partita corrente.
		/// </summary>
		public Checker Checker { get; }

        public HashFunctionsFactory HashFactory { get; }

		#endregion

		#region Constructors

		private MacinaEnvironment(GameMode mode, Checker checker)
		{
            GameMode = mode;
			Checker = checker;
            HashFactory = new HashFunctionsFactory(GameMode.Men);
        }

		#endregion

		#region Public methods

		public override string ToString()
		{
			return "Men(" + Men + "), Checker(" + Checker + ")";
		}

		#endregion
	}
}