﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.strategy;
using la_macina.it.unibo.ai.macina.model.strategy.implementations;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace la_macina
{
    class Engine
    {
        #region Members

        private readonly TcpClient _socket;
        private readonly Timer _timer;
        private State _currentState;
        private WorkingThread _worker;

        #endregion

        #region Constructors

        public Engine(string host, int port)
        {
            _socket = new TcpClient(host, port);
            _timer = new Timer(10000)
            {
                AutoReset = true,
                Enabled = true
            };
            _timer.Elapsed += OnTimeout;
            _worker = new WorkingThread(new MattStrat(5, .001f, new StateExpander(), MacinaEnvironment.GetInstance().GameMode.Heuristic), true);
        }

        #endregion

        #region Internal methods

        private void OnTimeout(object sender, ElapsedEventArgs args)
        {
            _worker.Stop();
			_timer.Stop();
        }

        private string WaitForState()
        {
            var buffer = new byte[1];
            _socket.GetStream().Read(buffer, 0, buffer.Length);
            buffer = new byte[buffer[0]];
            _socket.GetStream().Read(buffer, 0, buffer.Length);

            return Encoding.ASCII.GetString(buffer);
        }

        private State ReceiveState()
        {
            return State.Parse(WaitForState());
        }

        private string SendMove(Move m)
        {
            var moveAsString = m.ToString();

            var buffer = (byte)moveAsString.Length;
            var byteString = Encoding.ASCII.GetBytes(moveAsString);
            _socket.GetStream().WriteByte(buffer);
            _socket.GetStream().Flush();
            _socket.GetStream().Write(byteString, 0, byteString.Length);
            _socket.GetStream().Flush();
            return WaitForState();
        }

        private void WhiteCycle()
        {
            _timer.Start();
            _worker.ResumeSync();
            
            _currentState = State.Parse(SendMove(_worker.GetAndApplyBestMove()));

            _worker.Resume();
            _currentState = ReceiveState();
            _worker.Stop();

            _worker.SetOpponentChoice(_currentState);
        }

        private void BlackCycle()
        {
            _worker.Resume();
            _currentState = ReceiveState();
            _worker.Stop();

            _worker.SetOpponentChoice(_currentState);

            _timer.Start();
            _worker.ResumeSync();

            _currentState = State.Parse(SendMove(_worker.GetAndApplyBestMove()));
        }

        #endregion

        #region Public methods

        public void Start()
        {
            _currentState = ReceiveState();

            var ch = MacinaEnvironment.GetInstance().Checker;
            var imWhite = Checker.WHITE == ch;
            while (/* !_currentState.IsGoal() && */ true /* Finché non abbiamo vinto o perso */)
            {
                if (imWhite)
                    WhiteCycle();
                else BlackCycle();
            }
        }

        #endregion
    }
}
