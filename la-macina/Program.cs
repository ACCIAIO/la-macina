﻿using la_macina.it.unibo.ai.macina;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.evaluation;
using la_macina.it.unibo.ai.macina.model.grid;
using la_macina.it.unibo.ai.macina.model.strategy;
using la_macina.it.unibo.ai.macina.model.transposition;
using la_macina.it.unibo.ai.macina.model.transposition.rotation;
using la_macina.it.unibo.ai.macina.model.transposition.symmetry;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace la_macina
{
    class Program
    {
        private static void PrintHelp()
        {
            Console.WriteLine("+--------+\n|  HELP  |\n+--------+");
            Console.WriteLine("Syntax -> " + System.AppDomain.CurrentDomain.FriendlyName + "[color] [men]");
            Console.WriteLine("\t\t[color]\t-> \"black\" or \"white\"");
            Console.WriteLine("\t\t[men]\t-> valid values: 3, 6, 9, 12");
            Environment.Exit(0);
        }

        private static byte HandleMen(string str)
        {
            try
            {
                return Convert.ToByte(str);
            }
            catch
            {
                throw new ArgumentException("Invalid number format for argument " + str);
            }
        }

        private static Checker HandleCheckerArgument(string str)
        {
            if ("help" == str)
                PrintHelp();
            str = str.ToUpper();
            if ("BLACK" == str || "WHITE" == str)
            {
                return (Checker)Enum.Parse(typeof(Checker), str);
            }

            return Checker.NONE;
        }

        private static void HandleArgs(string[] args)
        {
            byte men = 9;
            Checker checker = Checker.WHITE;

            if (1 <= args.Length)
            {
                var tmp = HandleCheckerArgument(args[0]);
                if (Checker.NONE != tmp)
                    checker = tmp;
                else men = HandleMen(args[0]);
            }

            if(2 <= args.Length)  
                men = HandleMen(args[1]);

            MacinaEnvironment.CreateInstance(checker, men);
        }

        static void Main(string[] args)
        {
            HandleArgs(args);

            var env = MacinaEnvironment.GetInstance();
            var imWhite = Checker.WHITE == env.Checker;

            var stubPort = imWhite? 8000 : 8001;
            var serverPort = imWhite ? 5800 : 5801;

            Process p = new Process();
            p.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
            p.StartInfo.FileName = "java.exe";
            p.StartInfo.Arguments = "-jar macinaStub.jar " + serverPort + " " + stubPort;
            p.StartInfo.UseShellExecute = true;
            p.StartInfo.RedirectStandardOutput = false;
            p.StartInfo.RedirectStandardError = false;
            p.Start();

            var engine = new Engine("localhost", stubPort);

            engine.Start();

            Console.WriteLine();
			Console.WriteLine("END - Press Enter to exit.");
			Console.ReadLine();
        }
    }
}
