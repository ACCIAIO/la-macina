﻿//using la_macina.it.unibo.ai.macina.model.searchtree;
using la_macina.it.unibo.ai.macina.model;
using la_macina.it.unibo.ai.macina.model.strategy;
using System;
using System.Threading;

namespace la_macina.it.unibo.ai.macina
{
    interface IWorkingThread
    {
        void Start();
        void Stop();
        void ResumeSync();
        void Resume();
        Move GetAndApplyBestMove();
        void SetOpponentChoice(State resultingState);
    }

    /// <summary>
    /// Working thread è la classe che si occupa di effettuare la ricerca nel grafo.
    /// L'algoritmo di ricerca e l'euristica impiegata per l'eventuale valutazione
    /// dei nodi non sono competenza di questa classe: l'unico compito di questa
    /// classe è la corretta sincronizzazione del thread.
    /// 
    /// Si può molto probabilmente rendere più efficiente.
    /// </summary>
    class WorkingThread : IWorkingThread
    {
        private bool _timedOut = false;
        private ThreadStart _workingThreadLogic;
        private IExpansionStrategy _strategy;
        private bool _logStats;

        public WorkingThread(IExpansionStrategy strategy, bool logStats)
        {
            _strategy = strategy;
            _logStats = logStats;
            _workingThreadLogic = () =>
            {
                while (!_timedOut)
                {
                    strategy.ExpandNode();
                }
                if (_logStats)
                {
                    Console.WriteLine("\nEsplorato fino al livello: " + _strategy.Graph.MaxDepth);
                    Console.WriteLine("Generati " + _strategy.Graph.NodesCount + " nodi.");
                }
            };
        }
        
        /// <summary>
        /// Fa partire il ciclo di valutazione del working thread.
        /// </summary>
        public void Start()
        {
            new Thread(_workingThreadLogic).Start();
        }

        /// <summary>
        /// Ferma il working thread. Non garantisce una terminazione immediata!
        /// Tenere conto di questo e dimensionare il tempo da assegnare al thread
        /// in maniera oculata.
        /// </summary>
        public void Stop()
        {
            _timedOut = true;
        }

        /// <summary>
        /// Fa ripartire il ciclo di valutazione del working thread, ma senza
        /// dargli autonomia di controllo. Il flusso di esecuzione impiegato è quello
        /// del processo chiamante, che non riprende con la propria esecuzione fino
        /// al sopraggiungere di UN ALTRO processo che lo fermi (.Stop()).
        /// </summary>
        public void ResumeSync()
        {
            _timedOut = false;
            _workingThreadLogic();
        }

        /// <summary>
        /// Riprende il ciclo di calcolo del working thread.
        /// </summary>
        public void Resume()
        {
            _timedOut = false;
            Start();
        }

        /// <summary>
        /// Ritorna ed effettua la mossa migliore.
		/// La logica per la scelta è delegata alla strategia di espansione.
        /// </summary>
        /// <returns></returns>
        public Move GetAndApplyBestMove()
        {
			//DEBUG
			//Console.WriteLine("My state");
			Move bestMove = _strategy.GetBestMove();
			Console.WriteLine("Mossa: {0}, Player: {1}", bestMove, bestMove.Target);
			_strategy.SetCurrentState(_strategy.GetCurrentState().Evolve(bestMove));
			return bestMove;
        }

        /// <summary>
        /// Consente di specificare la scelta effettuata dall'avversario.  La logica
        /// per gli eventuali tagli all'albero sono delegate alla strategia di
        /// espansione.
        /// </summary>
        /// <param name="resultingState">Lo stato risultante dalla mossa dell'avversario.
        /// La scelta di indicare lo stato e non la mossa compiuta dall'avversario deriva
        /// del protocollo di specificato dal server di gioco.</param>
        public void SetOpponentChoice(State resultingState)
        {
			//Console.WriteLine("Opponent state");
            _strategy.SetCurrentState(resultingState);
        }
    }
}
